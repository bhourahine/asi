//#include <cstdbool>
#define _Bool bool
#include <dftbplus.h>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <vector>
#include <array>
#include <numeric>
#include <algorithm>
#include <stdexcept>
#include <iostream>
#include <complex>

#include "asi.h"

using namespace std;

namespace dftbp_asi_impl
{
  typedef struct {
    void *real_aux;
    ASI_dmhs_callback_t real_callback;
  } dmhs_callback_dftbp_aux_wrap;

  const char *asi_log_calls = getenv("ASI_LOG_CALLS");

  const double bohr = 0.52917721; // bohrs in 1 Ang.
  const double hartree = 1./27.2113845; // Hartree in 1 eV.

  bool ASI_initialized = false;
  DftbPlus calculator;
  DftbPlusInput input;
  int major, minor, patch;
  std::vector<std::array<double, 3> > atom_coords;
  std::vector<std::array<double, 3> > saved_total_forces;
  std::vector<std::array<double, 3> > saved_atomic_charges;
  double saved_stress_tensor[3*3];

  ASI_ext_pot_func_t ext_pot_func = 0;
  void *ext_pot_func_aux_ptr = 0;
  
  dmhs_callback_dftbp_aux_wrap hamiltonian_callback_aux_wrapper;
  dmhs_callback_dftbp_aux_wrap overlap_callback_aux_wrapper;
  dmhs_callback_dftbp_aux_wrap dm_callback_aux_wrapper;

  // Local functions

  void ext_pot_func_dftbp(void *refptr, double * /*dqatom unused*/, double *extpotatom)
  {
    int n = dftbp_get_nr_atoms(&calculator);
    assert(refptr == ext_pot_func_aux_ptr); // just in case
    ext_pot_func(ext_pot_func_aux_ptr, n, atom_coords[0].data(), extpotatom, 0);
    for (int i = 0; i < n; ++i)
    {
      extpotatom[i] *= -1;
    }
  }

  void ext_pot_grad_func_dftbp(void *refptr, double */*dqatom unused*/, double *extpotatomgrad)
  {
    int n = dftbp_get_nr_atoms(&calculator);
    assert(refptr == ext_pot_func_aux_ptr); // just in case
    ext_pot_func(ext_pot_func_aux_ptr, n, atom_coords[0].data(), 0, extpotatomgrad);
    for (int i = 0; i < n*3; ++i)
    {
      extpotatomgrad[i] *= -1;
    }  
  }
  
  /**
    Wrapper for callbacks for export from DFTB+.
    Always returns 0, because it is export callback, so it doesn't change anything.
  */
  void dmhs_callback_dftbp(void *aux_ptr, int iK, int iS, int *blacs_descr, const void *blacs_data, DftbPlusMatrixDescr *matrix_descr)
  {
    dmhs_callback_dftbp_aux_wrap &aux_wrap = *reinterpret_cast<dmhs_callback_dftbp_aux_wrap*>(aux_ptr);
    ASI_matrix_descr_t asi_m_descr;
    asi_m_descr.matrix_type = matrix_descr->matrix_type;
    asi_m_descr.storage_type = matrix_descr->storage_type;
    aux_wrap.real_callback(aux_wrap.real_aux, iK, iS, blacs_descr, blacs_data, &asi_m_descr);
  }
} // namespace for local names

#define LOG_FUNC if (dftbp_asi_impl::asi_log_calls) {std::cout << "\033[1;37m" << "ASI_LOG: " << __FUNCTION__ << "\033[0m" << std::endl;}

int ASI_flavour()
{
  return ASI_DFTBP;
}

void ASI_init(const char *inputpath, const char *outputfilename, int mpiComm)
{
  LOG_FUNC
  dftbp_api(&dftbp_asi_impl::major, &dftbp_asi_impl::minor, &dftbp_asi_impl::patch);
  if (mpiComm == -1)
  {
    dftbp_init(&dftbp_asi_impl::calculator, outputfilename);
  }
  else
  {
    dftbp_init_mpi(&dftbp_asi_impl::calculator, outputfilename, mpiComm);
  }
  dftbp_get_input_from_file(&dftbp_asi_impl::calculator, inputpath, &dftbp_asi_impl::input);
  dftbp_process_input(&dftbp_asi_impl::calculator, &dftbp_asi_impl::input);
  
  dftbp_asi_impl::ASI_initialized = true;
}

int ASI_get_basis_size()
{
  LOG_FUNC
  return dftbp_get_basis_size(&dftbp_asi_impl::calculator);
}

bool ASI_is_hamiltonian_real()
{
  LOG_FUNC
  return dftbp_is_hs_real(&dftbp_asi_impl::calculator);
}

void ASI_register_dm_callback(ASI_dmhs_callback_t dm_callback, void *aux_ptr)
{
  LOG_FUNC
  dftbp_asi_impl::dm_callback_aux_wrapper.real_callback = dm_callback;
  dftbp_asi_impl::dm_callback_aux_wrapper.real_aux = aux_ptr;
  dftbp_register_dm_callback(&dftbp_asi_impl::calculator, dftbp_asi_impl::dmhs_callback_dftbp, &dftbp_asi_impl::dm_callback_aux_wrapper);
}

void ASI_register_overlap_callback(ASI_dmhs_callback_t hs_callback, void *aux_ptr)
{
  LOG_FUNC
  dftbp_asi_impl::overlap_callback_aux_wrapper.real_callback = hs_callback;
  dftbp_asi_impl::overlap_callback_aux_wrapper.real_aux = aux_ptr;
  dftbp_register_s_callback(&dftbp_asi_impl::calculator, dftbp_asi_impl::dmhs_callback_dftbp, &dftbp_asi_impl::overlap_callback_aux_wrapper);
}

void ASI_register_hamiltonian_callback(ASI_dmhs_callback_t hs_callback, void *aux_ptr)
{
  LOG_FUNC
  
  dftbp_asi_impl::hamiltonian_callback_aux_wrapper.real_callback = hs_callback;
  dftbp_asi_impl::hamiltonian_callback_aux_wrapper.real_aux = aux_ptr;
  dftbp_register_h_callback(&dftbp_asi_impl::calculator, dftbp_asi_impl::dmhs_callback_dftbp, &dftbp_asi_impl::hamiltonian_callback_aux_wrapper);
}

/*
  coords assumed in Bohrs
*/
void ASI_set_atom_coords(const double *coords, int /*n_atoms unused*/)
{
  LOG_FUNC
  const int n = ASI_n_atoms();
  dftbp_asi_impl::atom_coords.resize(n);
 
  // copy and convert in bohrs // NOT CONVERT
  std::transform(coords, coords + 3*n, dftbp_asi_impl::atom_coords[0].data(), [](double x){return x/1.0;} );
  
  dftbp_set_coords(&dftbp_asi_impl::calculator, dftbp_asi_impl::atom_coords[0].data());
  
  // save in Angstroms
  std::copy(coords, coords + 3*n, dftbp_asi_impl::atom_coords[0].data());

}

/*
  coords assumed in Bohrs
*/
void ASI_set_geometry(const double *coords, int n_atoms, const double *lattice)
{
  LOG_FUNC
  const int n = ASI_n_atoms();
  assert(n == n_atoms);
  
  dftbp_asi_impl::atom_coords.resize(n);
 
  std::transform(coords, coords + 3*n, dftbp_asi_impl::atom_coords[0].data(), [](double x){return x/1.0;} );
  
  dftbp_set_coords_and_lattice_vecs(&dftbp_asi_impl::calculator, dftbp_asi_impl::atom_coords[0].data(), lattice);
  
  // save in Angstroms
  std::copy(coords, coords + 3*n, dftbp_asi_impl::atom_coords[0].data());

}


void ASI_set_external_potential(ASI_ext_pot_func_t ext_pot_func, void * ext_pot_func_aux_ptr)
{
  LOG_FUNC
  const int n = ASI_n_atoms();
  assert(dftbp_asi_impl::atom_coords.size() == n);
  std::vector<double> extpot(n);
  std::vector<std::array<double, 3> > extpotgrad(n);
  
  ext_pot_func(ext_pot_func_aux_ptr, n, dftbp_asi_impl::atom_coords[0].data(), extpot.data(), extpotgrad[0].data());
  
  for (int i = 0; i < n; ++i)
  {
    extpot[i] *= -1;
    extpotgrad[i][0] *= -1;
    extpotgrad[i][1] *= -1;
    extpotgrad[i][2] *= -1;
  }
  
  dftbp_set_external_potential(&dftbp_asi_impl::calculator, extpot.data(), extpotgrad[0].data());
}

void ASI_register_external_potential(ASI_ext_pot_func_t _ext_pot_func, void * _ext_pot_func_aux_ptr)
{
  LOG_FUNC
  dftbp_asi_impl::ext_pot_func = _ext_pot_func;
  dftbp_asi_impl::ext_pot_func_aux_ptr = _ext_pot_func_aux_ptr;
  
  dftbp_register_ext_pot_generator(&dftbp_asi_impl::calculator, _ext_pot_func_aux_ptr, dftbp_asi_impl::ext_pot_func_dftbp, dftbp_asi_impl::ext_pot_grad_func_dftbp);
}

void ASI_run()
{
  LOG_FUNC
  assert(dftbp_asi_impl::ASI_initialized && "ASI not initialized");

  double mermin_energy;
  dftbp_get_energy(&dftbp_asi_impl::calculator, &mermin_energy); 
}

double ASI_energy()
{
  LOG_FUNC
  double mermin_energy;
  dftbp_get_energy(&dftbp_asi_impl::calculator, &mermin_energy);
  return mermin_energy;
}

int ASI_n_atoms()
{
  LOG_FUNC
  return dftbp_get_nr_atoms(&dftbp_asi_impl::calculator);
}

int ASI_get_nspin()
{
  LOG_FUNC
  return dftbp_get_nr_spin(&dftbp_asi_impl::calculator);
}

int ASI_get_nkpts()
{
  LOG_FUNC
  return dftbp_nr_kpoints(&dftbp_asi_impl::calculator);
}

int ASI_get_n_local_ks()
{
  LOG_FUNC
  return dftbp_get_nr_local_ks(&dftbp_asi_impl::calculator);
}

int ASI_get_local_ks(int *local_ks)
{
  LOG_FUNC
  return dftbp_get_local_ks(&dftbp_asi_impl::calculator, local_ks);
}

const double * ASI_forces()
{
  LOG_FUNC
  dftbp_asi_impl::saved_total_forces.resize(ASI_n_atoms());
  dftbp_get_gradients(&dftbp_asi_impl::calculator, dftbp_asi_impl::saved_total_forces[0].data());
  for (size_t i = 0; i < dftbp_asi_impl::saved_total_forces.size(); ++i)
  {
    dftbp_asi_impl::saved_total_forces.at(i)[0] *= -1;
    dftbp_asi_impl::saved_total_forces.at(i)[1] *= -1;
    dftbp_asi_impl::saved_total_forces.at(i)[2] *= -1;
  }
  return dftbp_asi_impl::saved_total_forces[0].data();
}

const double * ASI_stress()
{
  LOG_FUNC
  dftbp_get_stress_tensor(&dftbp_asi_impl::calculator, dftbp_asi_impl::saved_stress_tensor);
  return dftbp_asi_impl::saved_stress_tensor;
}

const double* ASI_atomic_charges(int scheme)
{
  LOG_FUNC
  dftbp_asi_impl::saved_atomic_charges.resize(ASI_n_atoms());
  dftbp_get_gross_charges(&dftbp_asi_impl::calculator, dftbp_asi_impl::saved_atomic_charges[0].data());
  return dftbp_asi_impl::saved_atomic_charges[0].data();
}

void ASI_calc_esp(int n, const double *coords, double *potential, double *potential_grad)
{
  LOG_FUNC
  std::vector<double> potential_buf; // empty for a while
  
  if (not potential)
  {
    potential_buf.resize(n);
    potential = &(potential_buf[0]);
  }

  dftbp_get_elstat_potential(&dftbp_asi_impl::calculator, n, potential, coords); //need it in any way
  for(int i = 0; i < n; ++i)
  {
      potential[i] *= -1; // DFTB+ operates by electronic potential, so invertion is necessary to get conventional protonic potential
  }

  if (potential_grad) // TODO: replace with dftbp_get_potential_gradient
  {
    const double d = 0.001;
    std::vector<std::array<double, 3> > grad_coords(n*3);
    for(int i = 0; i < n; ++i)
    {
      for(int j = 0; j < 3; ++j)
      {
        grad_coords[i*3 + j] = {coords[i*3 + 0], coords[i*3 + 1], coords[i*3 + 2]};
        grad_coords[i*3 + j][j] += d;
      }
    }
    dftbp_get_elstat_potential(&dftbp_asi_impl::calculator, n*3, potential_grad, grad_coords[0].data()); 
    for(int i = 0; i < n; ++i)
    {
      for(int j = 0; j < 3; ++j)
      {
        potential_grad[i*3 + j] *=  -1;
        potential_grad[i*3 + j] = (potential_grad[i*3 + j] - potential[i]) / d;
      }
    }
  }
}

void ASI_finalize()
{
  LOG_FUNC
  dftbp_final(&dftbp_asi_impl::calculator);
}

