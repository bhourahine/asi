from mpi4py import MPI
import numpy as np
from ase.build import molecule
import os, sys
from ctypes import cast, py_object, CDLL, RTLD_GLOBAL
from asi4py import ASI_ASE_calculator
from time import sleep
from mpiprint import ordprint

ASI_LIB_PATH = os.environ['ASI_LIB_PATH']
asilib = CDLL(ASI_LIB_PATH, mode=RTLD_GLOBAL)

if asilib.ASI_flavour() == 1:
  def init_via_ase(asi):
    from ase.calculators.aims import Aims
    calc = Aims(xc='pw-lda', 
    )
    calc.write_input(asi.atoms)
else:
  def init_via_ase(asi):
    from ase.calculators.dftb import Dftb
    calc = Dftb(label='Some_cluster',
          Hamiltonian_SCC='Yes',
          Hamiltonian_MaxAngularMomentum_='',
          Hamiltonian_MaxAngularMomentum_O='"p"',
          Hamiltonian_MaxAngularMomentum_H='"s"',
          Parallel_='',
          Parallel_Groups=1,          # for k-points,spin-chanel parallelization
          Parallel_Blacs_='',
          Parallel_Blacs_BlockSize=4, # for BLACS distribution of matrices, decrease in case of: ERROR! -> Processor grid (1 x 2) too big (> 1 x 1)
          )
    calc.write_input(asi.atoms)

def esp(aux, n, coords, potential, potential_grad):
  coords = np.ctypeslib.as_array(coords, shape=(n, 3))
  atoms = cast(aux, py_object).value
  
  potential_requested = False
  if (potential):
    potential_requested = True
    potential = np.ctypeslib.as_array(potential, shape=(n, ))
    potential[:] = 0

  potential_grad_requested = False
  if (potential_grad):
    potential_grad_requested = True
    potential_grad = np.ctypeslib.as_array(potential_grad, shape=(n, 3))
    potential_grad[:,:] = 0
  
  if atoms.use_external_potential:
    MPI.COMM_WORLD.send(coords, dest=server_rank, tag=111)
    pot = MPI.COMM_WORLD.recv(source=server_rank, tag=222)
    pot_grad = MPI.COMM_WORLD.recv(source=server_rank, tag=333)
    
    if potential_requested:
      potential[:] = pot
    if potential_grad_requested:
      potential_grad[:] = pot_grad


def run_esp_server(atoms):
  assert MPI.COMM_WORLD.rank == server_rank, f"Error: MPI.COMM_WORLD.rank={MPI.COMM_WORLD.rank} server_rank={server_rank}"
  status = MPI.Status()
  while True:
    data = MPI.COMM_WORLD.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
    source = status.Get_source()
    tag = status.Get_tag()
    if tag==999:
      #print ("Stopping ESP server")
      return
    elif tag==111:
      atoms.calc.asi.mpi_comm.Barrier()  # calc_esp in DFTB+ MUST be called by all processes
      pot, pot_grad = atoms.calc.asi.calc_esp(data)
      MPI.COMM_WORLD.send(pot, dest=source, tag=222)
      MPI.COMM_WORLD.send(pot_grad, dest=source, tag=333)
    else:
      assert False, f"Unexpected tag == {tag} from source == {source}"

def stop_esp_server():
  assert MPI.COMM_WORLD.rank != server_rank, f"Error: MPI.COMM_WORLD.rank={MPI.COMM_WORLD.rank} server_rank={server_rank}"
  MPI.COMM_WORLD.send(123456, dest=server_rank, tag=999)
  #print ("stop_esp_server done")

assert MPI.COMM_WORLD.size == 2, f"Number of MPI tasks should be 2, but it is {MPI.COMM_WORLD.size}"

atoms1 = molecule('H2O')
atoms2 = molecule('H2O')
atoms2.translate([0,0, 4])

if sys.argv[1] == 'pair':
  atoms = atoms1 + atoms2
  atoms.calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, None, atoms)
  Epair = atoms.get_potential_energy()
  if MPI.COMM_WORLD.rank==0:
    print(f"Epair == {Epair:.6f}")
  sys.exit(0)

sys.argv[1] == 'SO'

color = MPI.COMM_WORLD.rank%2
asicomm = MPI.COMM_WORLD.Split(color = color)
#print ("color",color, MPI.COMM_WORLD.rank)
atoms = [atoms1, atoms2][color]

server_color = 0
server_rank = 0

atoms.calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, asicomm, atoms, work_dir=f'asi.temp.C.{color}')
#print ("atoms.calc",color, MPI.COMM_WORLD.rank)
atoms.calc.asi.register_external_potential(esp, atoms)
atoms.use_external_potential = False # disable external ESP for a while, for E0 calculation

E0 = atoms.get_potential_energy()
ordprint(f"E_0_{color} == {E0:.6f}")

for iii in range(5):
  if color==server_color:
    atoms.use_external_potential = False 
    run_esp_server(atoms)
  else:
      atoms.use_external_potential = True
      atoms.calc.calculate(atoms)   # force recalculation
      E1 = atoms.get_potential_energy()
      if asicomm.rank==0:
        print (f"{iii} {color} E_esp: {E1:.6f}, {E1-E0:.6f}" )
        stop_esp_server()
  sys.stdout.flush()
  MPI.COMM_WORLD.Barrier()
  #print ("Finish", MPI.COMM_WORLD.rank, color)
  sys.stdout.flush()
  MPI.COMM_WORLD.Barrier()
  server_color = 1 - server_color
  server_rank = 1 - server_rank

sys.stdout.flush()
MPI.COMM_WORLD.reduce(1) # instead of MPI.COMM_WORLD.Barrier() that doesn't work

ordprint(f"E_esp_{color} == {E1-E0:.6f}")

