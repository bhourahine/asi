import sys, os
import numpy as np

from ase.io import read, write
from ase.optimize import LBFGS
from ase.build import molecule
from asi4py.asecalc import ASI_ASE_calculator
from asi4py.dm import PredictFreeAtoms, PredictSeqDM

def init_via_ase(asi):
  from ase.calculators.aims import Aims
  calc = Aims(xc='pbe', 
    relativistic="atomic_zora scalar",
    compute_forces=True,
    density_update_method="density_matrix"
  )
  calc.write_input(asi.atoms)

atoms = molecule(sys.argv[1])

fa_dm_predictor = PredictFreeAtoms()
dm_predictor = PredictSeqDM(fa_dm_predictor, 10)

atoms.calc = ASI_ASE_calculator(os.environ['ASI_LIB_PATH'], init_via_ase, None, atoms)
dm_predictor.register_DM_init(atoms.calc.asi)
atoms.calc.asi.keep_density_matrix = True

def dyn_step():
  print (f"E = {atoms.get_potential_energy():6f} eV. Number of SCF steps: {atoms.calc.asi.dm_calc_cnt[1,1]}")
  dm_predictor.update_errs(atoms.calc.asi.dm_storage[(1,1)])
  dm_predictor.register_DM_init(atoms.calc.asi)
  atoms.calc.asi.keep_density_matrix = True

dyn = LBFGS(atoms)
dyn.attach(dyn_step, 1)
dyn.run(fmax=0.01)



