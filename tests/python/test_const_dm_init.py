import sys, os
import numpy as np

from ase.io import read, write
from ase.build import molecule
from asi4py.asecalc import ASI_ASE_calculator
from asi4py.dm import PredictConstAtoms

def init_via_ase(asi):
  from ase.calculators.aims import Aims
  calc = Aims(xc="pbe", 
    relativistic="atomic_zora scalar",
    density_update_method="density_matrix"
  )
  calc.write_input(asi.atoms)

atoms = molecule(sys.argv[1])
initial_DM = np.load(f"{sys.argv[1]}.npz")["DM"]

dm_predictor = PredictConstAtoms(atoms, initial_DM)

atoms.calc = ASI_ASE_calculator(os.environ["ASI_LIB_PATH"], init_via_ase, None, atoms)
dm_predictor.register_DM_init(atoms.calc.asi)
atoms.calc.asi.keep_density_matrix = True
atoms.rattle(stdev=0.001, seed=123)

print(f'E = {atoms.get_potential_energy():.6f}')
print("Number of SCF steps:", atoms.calc.asi.dm_calc_cnt[1,1])


