import sys, os
import numpy as np
from ctypes import CDLL, RTLD_GLOBAL

from ase.build import molecule
from ase.io import read, write
from asi4py.asecalc import ASI_ASE_calculator
from ase import units

# read path to ASI-implementing shared library from environment variable
ASI_LIB_PATH = os.environ['ASI_LIB_PATH']
# load the ASI lib for direct C API calls, 
# RTLD_GLOBAL is necessary for MPI
asilib = CDLL(ASI_LIB_PATH, mode=RTLD_GLOBAL)

# use direct ASI API call to distiguish ASI implementations
# ASI_flavour()==1 for FHI-aims; ASI_flavour()==2 for DFTB+
# See documentation for details: https://pvst.gitlab.io/asi/asi_8h.html#a3db29227112f71d020732b18db023315
if asilib.ASI_flavour() == 1:
  # FHI-aims initializer via ase.calculators.aims.Aims calculator
  def init_via_ase(asi):
    from ase.calculators.aims import Aims
    calc = Aims(xc='pbe', 
      relativistic="atomic_zora scalar",
      occupation_type="gaussian 0.010",
      density_update_method="density_matrix"
    )
    calc.write_input(asi.atoms)
else:
  # DFTB+ initializer via ase.calculators.dftb.Dftb calculator
  def init_via_ase(asi):
    from ase.calculators.dftb import Dftb
    calc = Dftb(label='Some_cluster',
          Hamiltonian_SCC='Yes',
          Hamiltonian_MaxAngularMomentum_='',
          Hamiltonian_MaxAngularMomentum_O='"p"',
          Hamiltonian_MaxAngularMomentum_H='"s"',
          Hamiltonian_ASI_='',
          Hamiltonian_ASI_AsiModifiesModel = "No")
    calc.write_input(asi.atoms)

atoms = molecule('H2O')

# initialize ASI library via ASE calculators 
atoms.calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, None, atoms)

# Ask to save density, Hamiltonian, and overlap matrices
# See documentation for details: https://pvst.gitlab.io/asi/reference.html#asi4py.pyasi.ASIlib.keep_density_matrix
atoms.calc.asi.keep_density_matrix = True
atoms.calc.asi.keep_hamiltonian = True
atoms.calc.asi.keep_overlap = True

# Get number of basis functions via asi4py wrapper: https://pvst.gitlab.io/asi/reference.html#asi4py.pyasi.ASIlib.n_basis
print(f'basis size = {atoms.calc.asi.n_basis}')

# Do actual calculations (ASI_ASE_calculator calls ASI_run() underneath)
print(f'E = {atoms.get_potential_energy():.6f}')

# Get saved matrices. Index (1,1) means 1st k-point and 1st spin channel 
# See documentation for details: https://pvst.gitlab.io/asi/reference.html#asi4py.pyasi.ASIlib.keep_density_matrix
S = atoms.calc.asi.overlap_storage[(1,1)]
H = atoms.calc.asi.hamiltonian_storage[(1,1)]
DM = atoms.calc.asi.dm_storage.get((1,1), None)

# Do simple arithmetics on matrices to get electronic structure quantities
print(f'Total number of electrons = {np.sum(S*DM.T):.6f}')
print(f'Sum of eigenvalues  = {np.sum(H*DM.T):.6f}')
