import sys, os
import numpy as np

from ase.io import read, write
from ase.build import molecule
from asi4py.asecalc import ASI_ASE_calculator
from asi4py.dm import PredictFreeAtoms

def init_via_ase(asi):
  from ase.calculators.aims import Aims
  calc = Aims(xc='pbe', 
    relativistic="atomic_zora scalar",
    density_update_method="density_matrix"
  )
  calc.write_input(asi.atoms)

atoms = molecule(sys.argv[1])

atoms.calc = ASI_ASE_calculator(os.environ['ASI_LIB_PATH'], init_via_ase, None, atoms)
atoms.calc.asi.keep_density_matrix = True
dm_predictor = PredictFreeAtoms()

dm_predictor.register_DM_init(atoms.calc.asi)
'''
The line above is equivalent to:
  atoms.calc.asi.register_DM_init(PredictFreeAtoms.dm_init_callback, dm_predictor)
'''

print(f'E = {atoms.get_potential_energy():.6f}')

DM = atoms.calc.asi.dm_storage[(1,1)]
print("Number of SCF steps:", atoms.calc.asi.dm_calc_cnt[1,1])

np.savez(f"{sys.argv[1]}.npz", DM=DM)
