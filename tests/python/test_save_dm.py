import sys, os
import numpy as np

from ase.io import read, write
from ase import Atoms
from asi4py.asecalc import ASI_ASE_calculator
from asi4py.dm import save_atomic_dm

def init_via_ase(asi):
  from ase.calculators.aims import Aims
  calc = Aims(xc='pbe', 
    relativistic="atomic_zora scalar",
    density_update_method="density_matrix"
  )
  calc.write_input(asi.atoms)

elem = sys.argv[1]

print(f'Saving {elem} DM in basis from {os.environ["AIMS_SPECIES_DIR"]} into {os.environ["ASI_FREE_ATOM_DMS"]} directory')

atoms = Atoms(elem)

atoms.calc = ASI_ASE_calculator(os.environ['ASI_LIB_PATH'], init_via_ase, None, atoms)
atoms.calc.asi.keep_density_matrix = True

print(f'E = {atoms.get_potential_energy():.6f}')

DM = atoms.calc.asi.dm_storage[(1,1)]

save_atomic_dm(DM, elem)

