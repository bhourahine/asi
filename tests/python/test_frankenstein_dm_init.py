import sys, os
import numpy as np

from ase.io import read
from ase import Atoms
from ase.build import molecule
from asi4py.asecalc import ASI_ASE_calculator
from asi4py.dm import PredictConstAtoms, PredictFreeAtoms, PredictFrankensteinDM

def init_via_ase(asi):
  from ase.calculators.aims import Aims
  calc = Aims(xc="pbe", 
    relativistic="atomic_zora scalar",
    density_update_method="density_matrix"
  )
  calc.write_input(asi.atoms)

mol = molecule("CO2")
metal = Atoms("Cu")
metal.translate([2.5,0,0])
atoms = mol + metal

mol_DM = np.load(f"CO2.npz")["DM"]

mol_dm_predictor = PredictConstAtoms(mol, mol_DM)
#mol_dm_predictor = PredictFreeAtoms()
metal_dm_predictor = PredictFreeAtoms()

dm_predictor = PredictFrankensteinDM([ (mol_dm_predictor, [0,1,2]), (metal_dm_predictor, [3]) ])

atoms.calc = ASI_ASE_calculator(os.environ["ASI_LIB_PATH"], init_via_ase, None, atoms)
dm_predictor.register_DM_init(atoms.calc.asi)
atoms.calc.asi.keep_density_matrix = True

print(f'E = {atoms.get_potential_energy():.6f}')
print("Number of SCF steps:", atoms.calc.asi.dm_calc_cnt[1,1])


