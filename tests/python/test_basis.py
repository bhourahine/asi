import sys, os
import numpy as np
from ctypes import CDLL, RTLD_GLOBAL

from ase.build import molecule
from ase.io import read, write
from asi4py.asecalc import ASI_ASE_calculator
from ase import units

ASI_LIB_PATH = os.environ['ASI_LIB_PATH']
asilib = CDLL(ASI_LIB_PATH, mode=RTLD_GLOBAL)

if asilib.ASI_flavour() == 1:
  def init_via_ase(asi):
    from ase.calculators.aims import Aims
    calc = Aims(xc='pw-lda', 
      sc_accuracy_forces=5e-2, 
      species_dir=os.environ["AIMS_SPECIES_DIR"] + '/../../defaults_2010/light',
      relativistic='none',
    )
    calc.write_input(asi.atoms)
else:
  def init_via_ase(asi):
    from ase.calculators.dftb import Dftb
    calc = Dftb(label='Some_cluster',
          Hamiltonian_SCC='Yes',
          Hamiltonian_MaxAngularMomentum_='',
          Hamiltonian_MaxAngularMomentum_O='"p"',
          Hamiltonian_MaxAngularMomentum_H='"s"')
    calc.write_input(asi.atoms, properties=['forces'])


atoms = molecule('H2O')
ASI_LIB_PATH = os.environ['ASI_LIB_PATH']
calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, None, atoms)
print (calc.asi.n_basis)
print (calc.asi.basis_atoms)
atoms.calc = calc
print (f'{atoms.get_potential_energy():.3f}')


