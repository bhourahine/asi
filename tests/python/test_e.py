import sys, os
import numpy as np
ASI_LIB_PATH = os.environ['ASI_LIB_PATH']
from mpi4py import MPI

from ase.build import molecule
from asi4py.asecalc import ASI_ASE_calculator
from mpiprint import parprint, ordprint

def init_via_ase(asi):
  from ase.calculators.aims import Aims
  calc = Aims(xc='pbe', 
    relativistic="atomic_zora scalar",
    occupation_type="gaussian 0.010",
    sc_accuracy_eev=1E-3,
    sc_accuracy_rho=1e-05,
    sc_accuracy_etot=1e-06,
    #sc_accuracy_forces=1e-1, # just to enable force calculation
    postprocess_anyway = True,
    tier = [1, 2],
  )
  calc.write_input(asi.atoms)

atoms = molecule('H2O')
atoms.calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, MPI.COMM_WORLD, atoms)
parprint(f'E = {atoms.get_potential_energy():.6f}')

