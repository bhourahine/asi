#include <sstream>

struct pvst_ss
{
    std::ostringstream  ss;
    std::string str() {return ss.str();}
    template<typename T>
    pvst_ss& operator<<(const T &t)
    {
        this->ss << t;
        return *this;
    }
};

#define STR(X) ( ( pvst_ss() << X ).str() )


uint64_t rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return (((uint64_t)hi << 32) | lo) / 1000;
}

double kill_negative_zero(double x, double eps=1e-6)
{
    return fabs(x) < eps ? 0.0 : x;
}

std::complex<double> kill_negative_zero(std::complex<double> x, double eps=1e-6)
{
  double xr = kill_negative_zero(x.real(), eps);
  double xi = kill_negative_zero(x.imag(), eps);
  return std::complex<double>(xr, xi);
}
