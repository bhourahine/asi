#!/bin/sh

set -e

mkdir -p asi.temp
export OMP_NUM_THREADS=1
$MPIEXEC -n 2 $TESTING_PYTHON -u $PYTESTS/test_scmpi.py SC | tee asi.temp/test1.log
diff --color -s test1.log asi.temp/test1.log

$MPIEXEC -n 2 $TESTING_PYTHON -u $PYTESTS/test_scmpi.py pair | tee asi.temp/test2.log
diff --color -s test2.log asi.temp/test2.log

