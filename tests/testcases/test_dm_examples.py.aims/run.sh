#!/bin/sh

set -e
ulimit -s unlimited

rm -rf asi.temp
mkdir -p asi.temp

mkdir -p free_atom_dms

export ASI_FREE_ATOM_DMS=${PWD}/free_atom_dms
export AIMS_SPECIES_DIR=${AIMS_SPECIES_DIR}/../../defaults_2020/light

$TESTING_PYTHON -u $PYTESTS/test_save_dm.py H | grep -e 'E =' | tee -a asi.temp/test1.log
$TESTING_PYTHON -u $PYTESTS/test_save_dm.py O | grep -e 'E =' | tee -a asi.temp/test1.log
$TESTING_PYTHON -u $PYTESTS/test_save_dm.py C | grep -e 'E =' | tee -a asi.temp/test1.log
$TESTING_PYTHON -u $PYTESTS/test_save_dm.py Cu | grep -e 'E =' | tee -a asi.temp/test1.log

$TESTING_PYTHON -u $PYTESTS/test_free_atom_dm_init.py CH4 | tee -a asi.temp/test1.log
$TESTING_PYTHON -u $PYTESTS/test_free_atom_dm_init.py CO2 | tee -a asi.temp/test1.log

grep -e 'End self' asi.temp/asi.log | wc -l | tee -a asi.temp/test1.log

$TESTING_PYTHON -u $PYTESTS/test_const_dm_init.py CH4 | tee -a asi.temp/test1.log

$TESTING_PYTHON -u $PYTESTS/test_frankenstein_dm_init.py | tee -a asi.temp/test1.log

$TESTING_PYTHON -u $PYTESTS/test_seq_dm_init.py CH3CHO | grep -v LBFGS | tee -a asi.temp/test1.log

diff --color -s asi.temp/test1.log test1.log

