#!/bin/sh

set -e
ulimit -s unlimited

mkdir -p asi.temp

$TESTING_PYTHON -u $PYTESTS/test_basis.py | tee asi.temp/test1.log
diff --color -s test1.log asi.temp/test1.log
