#!/bin/sh

## Environment setup
## Modify to match you environment
. ~/prg/regr/ve311/bin/activate
python3 -m pip install asi4py
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/opt/dftbp-mpi-py/lib/
export ASI_LIB_PATH=$HOME/prg/asi/install/lib/libasidftbp.so
## End of Environment setup

echo "Simple DFTB+ run via ASI API with bands output"
python3 -u simple_bands.py ../Input_Files/geometry.in

echo "DFTB+ run via ASI API with imported Hamiltonian and overlap matrices"
python3 -u imported_bands.py ../Input_Files/geometry.in


