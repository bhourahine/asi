#!/bin/sh

## Environment setup
## Modify to match you environment

# Activate Python virtualenv:
. ~/prg/regr/ve311/bin/activate

# Install asi4py if necessary
python3 -m pip install asi4py

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/opt/dftbp-mpi-py/lib/
export ASI_LIB_PATH=$HOME/prg/asi/install/lib/libasidftbp.so
export MPIEXEC=mpiexec
export DFTB_PREFIX=$HOME/opt/dftbp-mpi-py/slakos/origin/mio-1-1/
## End of Environment setup

echo "Self-consisten electrostatic embedding:"
$MPIEXEC -n 2 python3 -u self_consistent_es_embedding.py SC

echo "Single-system calcultion for control"
$MPIEXEC -n 2 python3 -u self_consistent_es_embedding.py pair


