import sys, os
from shutil import copyfile
import numpy as np
from scipy.linalg import eigh

from ase.build import molecule, bulk
from ase.io import read, write
from asi4py.asecalc import ASI_ASE_calculator
from mpi4py import MPI
from ctypes import POINTER, byref, c_int, c_int64, c_int32, c_bool, c_char_p, c_double, c_void_p, CFUNCTYPE, py_object, cast, byref
from scalapack4py.blacsdesc import ctypes2ndarray

def plot_bands(bands, ylabel='eV'):
  import matplotlib.pylab as plt

  plt.axhline(y=0, ls='--', color='k')
  plt.plot(bands[:, :], color='blue')
  plt.xticks(np.cumsum([1, 49, 49, 49, 49, 49, 49]), 'WXGLKGL')
  plt.ylabel(ylabel)
  plt.grid(axis='x')
  plt.show()

ASI_LIB_PATH = os.environ['ASI_LIB_PATH']

def Lines_kpts():
  return ''' Klines {
     1   0.25  0.50  0.75  # W
    49   0.00  0.50  0.50  # X
    49   0.00  0.00  0.00  # G
    49   0.50  0.50  0.50  # L
    49   0.375 0.375 0.75  # K
    49   0.00  0.00  0.00  # G
    49   0.50  0.50  0.50  # L
   }
'''

def MP_kpts(l):
  '''
    DFTB+ manual: 
    The original l1 ×l2 ×l3 Monkhorst-Pack sampling [61] for cubic lattices corresponds to a uniform
    extension of the lattice. where si is 0.0, if li is odd, and si is 0.5 if li is even
  '''
  s = [0.0 if li%2==1 else 0.5 for li in l]
  
  return f"SupercellFolding {{ \n {l[0]} 0 0 \n 0 {l[1]} 0 \n 0 0 {l[2]} \n {s[0]:.1f} {s[1]:.1f} {s[2]:.1f}\n}}"

def init_via_ase(asi):
  from ase.calculators.dftb import Dftb
  calc = Dftb(label='Al_crystall',
        #kpts=(2,2,2),
        Hamiltonian_SCC='No',
        Hamiltonian_KPointsAndWeights = Lines_kpts() ,# MP_kpts((12,12,12)),
        Hamiltonian_MaxAngularMomentum_='',
        Hamiltonian_MaxAngularMomentum_Al='"d"', # 9 basis function
        WriteBandOut='Yes',
        WriteChargesAsText='Yes',
        WriteNetCharges='Yes')
  calc.write_input(asi.atoms, properties=['forces'])
  copyfile('../Al-Al.skf', 'Al-Al.skf')


def matrix_callback(aux, iK, iS, descr, data):
  try:
    asi, storage_dict, label = cast(aux, py_object).value
    data_shape = (asi.n_basis,asi.n_basis) if asi.is_hamiltonian_real else (asi.n_basis,asi.n_basis, 2)
    data = ctypes2ndarray(data, data_shape)
    data = data.T
    if label == "my-H-callback":
      data[...] = aims_H[iK-1]
    else:
      data[...] = aims_S[iK-1]
  except Exception as eee:
    print (f"Something happened in ASI matrix_callback {label}: {eee}\nAborting...")
    MPI.COMM_WORLD.Abort(1)

data = np.load('bands.npz')
aims_H = data['bands_H']
aims_S = data['bands_S']
print (aims_H.shape)

atoms = read(sys.argv[1])

atoms.calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, MPI.COMM_WORLD, atoms)
assert atoms.calc.asi.n_basis == 9, f'atoms.calc.asi.n_basis = {atoms.calc.asi.n_basis}' # expected basis size
assert atoms.calc.asi.n_basis == aims_H[0].shape[0]
S_dict={}
atoms.calc.asi.register_overlap_callback(matrix_callback, (atoms.calc.asi, S_dict, "my-S-callback"))
H_dict={}
atoms.calc.asi.register_hamiltonian_callback(matrix_callback, (atoms.calc.asi, H_dict, "my-H-callback"))


E = atoms.get_potential_energy()
print(f'E = {E:.6f}')

os.system("PYTHONPATH=~/opt/dftbp-mpi-py/lib/python3.9/site-packages/  ~/opt/dftbp-mpi-py/bin/dp_bands asi.temp/band.out bbb")
bands = np.loadtxt('bbb_tot.dat')[:, 1:]
plot_bands(bands[:, 5:]) # cut off 5 lowest bands
