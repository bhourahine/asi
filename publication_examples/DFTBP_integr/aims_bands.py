import sys, os
os.environ['OMP_NUM_THREADS'] = "1"
from pathlib import Path

work_dir=Path("./").resolve()
os.environ['AIMS_SPECIES_DIR'] = str(work_dir)
# Set minimal basis set for Al (9 basis functions). 
# Adopted from the standart FHI-aims's defaults_2020/light basis set.
with open(f'{work_dir}/13_Al_default','w') as f:
 f.write("""
  species        Al
#     global species definitions
    nucleus             13
    mass                26.9815386
#
    l_hartree           4
#
    cut_pot             3.5          1.5  1.0
    basis_dep_cutoff    1e-4
#
    radial_base         41 5.0
    radial_multiplier   1
    angular_grids       specified
      division   0.6594  110
      division   0.8170  194
      division   0.9059  302
      outer_grid   302
#  Definition of "minimal" basis
#     valence basis states
    valence      3  s   2.
    valence      3  p   1.
#     ion occupancy
    ion_occ      3  s   1.
    ion_occ      2  p   6.
#  "First tier" - improvements: -199.47 meV to -10.63 meV
#     ionic 3 d auto
#     ionic 3 p auto
#     hydro 4 d 4.7
#     ionic 3 s auto
 """)

import numpy as np
from scipy.linalg import eigh

from ase.build import molecule, bulk
from ase.io import read, write
from asi4py.asecalc import ASI_ASE_calculator
from ctypes import POINTER, c_int32, py_object, cast

from mpi4py import MPI


ASI_LIB_PATH = os.environ['ASI_LIB_PATH']

def init_via_ase(asi):
  from ase.calculators.aims import Aims
  calc = Aims(xc='pbe', 
    relativistic="atomic_zora scalar",
    occupation_type="gaussian 0.10",
    sc_accuracy_etot=1e-06,
    sc_accuracy_eev=1e-5,
    k_grid=(2,2,2),
    density_update_method="density_matrix",
    output=[  "band 0.25  0.50  0.75 0.00  0.50  0.50 50 W X", 
              "band 0.00  0.50  0.50 0.00  0.00  0.00 50 X Gamma",
              "band 0.00  0.00  0.00 0.50  0.50  0.50 50 Gamma L",
              "band 0.50  0.50  0.50 0.375 0.375 0.75 50 L K",
              "band 0.375 0.375 0.75 0.00  0.00  0.00 50 K Gamma",
              "band 0.00  0.00  0.00 0.50  0.50  0.50 50 Gamma L",])
  calc.write_input(asi.atoms)

def matrix_saving_callback(aux, iK, iS, descr, data):
  try:
    asi, storage_dict, label = cast(aux, py_object).value
    data_shape = (asi.n_basis,asi.n_basis) if asi.is_hamiltonian_real else (asi.n_basis,asi.n_basis, 2)
    data = asi.scalapack.gather_numpy(descr, data, data_shape)
    i_band = asi.lib.ASI_get_named_data("i-band".encode('UTF-8'))[0]
    if data is not None:
      assert len(data.shape) == 2
      storage_dict[(i_band, iK, iS)] = data.copy()
  except Exception as eee:
    print (f"Something happened in ASI matrix_saving_callback {label}: {eee}\nAborting...")
    MPI.COMM_WORLD.Abort(1)


print("""Usage example:
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/aims/build-so-docker/install/lib/ ASI_LIB_PATH=/aims/build-so-docker/install/lib/libaims.so $MPICH_HOME/bin/mpirun -n 1 python3 -u aims_bands.py ../Input_Files/geometry.in
""")

atoms = read(sys.argv[1])
atoms.calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, MPI.COMM_WORLD, atoms)

atoms.calc.asi.lib.ASI_get_named_data.restype = POINTER(c_int32) # for asi.lib.ASI_get_named_data("i-band".encode('UTF-8')) in matrix_saving_callback
n_basis = atoms.calc.asi.n_basis
print ("basis size: ", n_basis)

S_dict={}
atoms.calc.asi.register_overlap_callback(matrix_saving_callback, (atoms.calc.asi, S_dict, "my-S-callback"))

H_dict={}
atoms.calc.asi.register_hamiltonian_callback(matrix_saving_callback, (atoms.calc.asi, H_dict, "my-H-callback"))

E = atoms.get_potential_energy()
print(f'E = {E:.6f}')

bands = []
bands_H = []
bands_S = []
for i_band, iK, iS in S_dict:
  S = S_dict[(i_band, iK, iS)]
  H = H_dict[(i_band, iK, iS)]
  iKbp = (i_band-1)*50 + iK #index along band path
  iKbp_dftbp = iKbp - min(iKbp // 50, 5) # index skipping repeating points on band conjunctions

  if (i_band > 0) and ((iKbp == 300) or (iKbp  % 50 != 0)):
    bands_H.append(H)
    bands_S.append(S)

np.savez_compressed("bands.npz", bands_S=bands_S, bands_H=bands_H)
