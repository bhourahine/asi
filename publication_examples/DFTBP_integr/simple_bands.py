import sys, os
from shutil import copyfile
import numpy as np
from scipy.linalg import eigh

from ase.build import molecule, bulk
from ase.io import read, write
from asi4py.asecalc import ASI_ASE_calculator
from mpi4py import MPI
from ctypes import POINTER, byref, c_int, c_int64, c_int32, c_bool, c_char_p, c_double, c_void_p, CFUNCTYPE, py_object, cast, byref
from scalapack4py.blacsdesc import ctypes2ndarray

def plot_bands(bands, ylabel='eV'):
  import matplotlib.pylab as plt

  plt.axhline(y=0, ls='--', color='k')
  plt.plot(bands[:, :], color='blue')
  plt.xticks(np.cumsum([1, 49, 49, 49, 49, 49, 49]), 'WXGLKGL')
  plt.ylabel(ylabel)
  plt.grid(axis='x')
  plt.show()

ASI_LIB_PATH = os.environ['ASI_LIB_PATH']

def Lines_kpts():
  return ''' Klines {
     1   0.25  0.50  0.75  # W
    49   0.00  0.50  0.50  # X
    49   0.00  0.00  0.00  # G
    49   0.50  0.50  0.50  # L
    49   0.375 0.375 0.75  # K
    49   0.00  0.00  0.00  # G
    49   0.50  0.50  0.50  # L
   }
'''

def init_via_ase(asi):
  from ase.calculators.dftb import Dftb
  calc = Dftb(label='Al_crystall',
        #kpts=(2,2,2),
        Hamiltonian_SCC='No',
        Hamiltonian_KPointsAndWeights = Lines_kpts() ,
        Hamiltonian_MaxAngularMomentum_='',
        Hamiltonian_MaxAngularMomentum_Al='"d"', # 9 basis function
        WriteBandOut='Yes',
        WriteChargesAsText='Yes',
        WriteNetCharges='Yes')
  calc.write_input(asi.atoms, properties=['forces'])
  copyfile('../Al-Al.skf', 'Al-Al.skf')

atoms = read(sys.argv[1])

atoms.calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, MPI.COMM_WORLD, atoms)

E = atoms.get_potential_energy()
print(f'E = {E:.6f}')

os.system("PYTHONPATH=~/opt/dftbp-mpi-py/lib/python3.9/site-packages/  ~/opt/dftbp-mpi-py/bin/dp_bands asi.temp/band.out bbb")
bands = np.loadtxt('bbb_tot.dat')[:, 1:]
plot_bands(bands[:, :])
