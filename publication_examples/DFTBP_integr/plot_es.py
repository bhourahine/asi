import numpy as np, matplotlib.pylab as plt, io


E0 = -110.960395 # single H2O molecule energy, eV

'''
Columns:
  # distance between molecules, Ang
  # total energy of a pair of H2O molecules, eV
  # energy of self-consistent electrostatic interaction via ASI API, eV
'''
data = """
2.5	-221.908968336599		-0.159745
2.6	-221.963560342986		-0.145871
2.7	-221.991274643218		-0.133369
2.8	-222.003160563755		-0.122115
2.9	-222.006188871247		-0.111986
3	-222.00459308349		-0.102866
3.1	-222.000834243821		-0.094647
3.2	-221.996267271392		-0.087232
3.3	-221.991584189612		-0.080533
3.4	-221.987099335396		-0.074472
3.5	-221.982924982602		-0.06898
3.6	-221.979075907368		-0.063994
3.7	-221.975528308246		-0.059461
3.8	-221.972246978524		-0.055334
3.9	-221.969199326326		-0.051568
4	-221.966358936268		-0.048127
4.5	-221.954696142453		-0.034761
5	-221.946357623933		-0.025857
5.5	-221.940404678706		-0.019721
6	-221.936111620958		-0.015366
6.5	-221.932963749612		-0.012196
7	-221.930612920906		-0.009836
"""


data = np.loadtxt(io.StringIO(data))
d = data[:, 0]
Epair = data[:, 1] - 2*E0
Easi = data[:, 2] 

plt.plot(d, Epair, label='$E_{total}$, eV')
plt.plot(d, Easi, label='$E_{ESP}$, eV')
plt.xlabel('d, $\AA$')
plt.ylabel('intermolecular interaction, eV')
plt.legend()
plt.show()
