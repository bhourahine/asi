from subprocess import run
import sys

def get_pypi_versions(pkgname):
  msg = run(['pip', 'install', f'{pkgname}==XXX'], capture_output=True).stderr
  msg = str(msg)
  keyword = '(from versions: '
  i = msg.find(keyword)
  msg = msg[i+len(keyword):]
  i = msg.find(')') # closing braket of versions list
  msg = msg[:i]
  versions = [ tuple(int (i) for i in vs.strip().split('.')) for vs in msg.split(',')]
  return versions

def pyproject_version(pyproject_path):
  try:
    from build import toml_loads
  except ImportError:
    from toml import loads as toml_loads
  vs = toml_loads(open(pyproject_path).read())['project']['version']
  return tuple(int (i) for i in vs.strip().split('.'))

local_version = pyproject_version('pyasi/pyproject.toml')
print ("local_version:", local_version)
pypi_versions = get_pypi_versions('asi4py')
print ("pypi_versions:", pypi_versions)
print (local_version in pypi_versions)

