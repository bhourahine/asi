# Contributing Guidelines

ASI is a C-style API that can be used by any atomistic modelling software.
The ASI API specification is provided in the [`asi.h`](https://gitlab.com/pvst/asi/-/blob/master/include/asi.h) file in the [ASI repository](https://gitlab.com/pvst/asi).
The [`asi.h`](https://gitlab.com/pvst/asi/-/blob/master/include/asi.h) file contains declarations of plain C functions and data types along with Doxygen-style documentation.
The HTML-documentation generated from this file can be found here on the [ASI documentation website](https://pvst.gitlab.io/asi/).

## How to call the ASI API

If you are going to implement ASI API in some code, you should provide a dynamically linking library (shared object file named as `lib*.so`) that defines ASI functions that behave as described in the [specification](https://gitlab.com/pvst/asi/-/blob/master/include/asi.h). Although a complete implementation of all ASI functions is always desirable for the greater good of the developers and users community, a partial ASI API implementation is also welcomed if it enables an interesting use case, scenario, or workflow.

For example, currently there are two codes using the ASI API: [FHI-aims](https://fhi-aims.org/) and [DFTB+](https://dftbplus.org/) [(in a separate branch, for a while)](https://github.com/PavelStishenko/dftbplus/tree/ASI_v1.3). FHI-aims can be build as a single shared library `libaims.so` that defines ASI functions. On the other hand, the DFTB+ implementation of ASI should be built as a separate `libasidftbp.so` file that dynamically links with the main DFTB+ library `libdftbplus.so`. Both of these implementations of the ASI API are not complete: DFTB+ does not support `ASI_register_dm_init_callback()`, FHI-aims implementation supports only Hamiltonian export via `ASI_register_hamiltonian_callback()` (import is not supported now).

To deal with unavoidable peculiarities of ASI API implementations, there is the `ASI_flavour()` function that returns an integer constant that is unique for each ASI implementation. If you are going to implement ASI for a new code, please submit a merge request with a new flavour constant in `asi.h` nearby existing flavours: `ASI_FHI_AIMS `and `ASI_DFTBP`.


## Contributing and issues reporting

This repository contains ASI API specification, `asi4py` Python wrapper for ASI API, and DFTB+ ASI implementation. 

Therefore this repository is a good place to submit reports and fixes for the following types of issues:

* issues that affect all ASI implementations (ASI documentation, specification,)
* DFTB+ ASI implementation issues
* the `asi4py` Python wrapper issue
* initial suggestions for the new ASI implementations
* suggestions for ASI API specification improvements

This repository is not the best place for the following issues:

* FHI-aims ASI implementation issues
