
elem_tiers = {'O':1, 'H':2, 'C':1}
species_set = 'defaults_2010/tight'

def init_via_ase(asi, forces=False):
  import os
  from ase.calculators.aims import Aims
  calc = Aims(xc='pbe', 
    relativistic='atomic_zora scalar',
    occupation_type="gaussian 0.010",
    sc_accuracy_eev=1E-3,
    sc_accuracy_rho=1e-05,
    sc_accuracy_etot=1e-06,
    charge_mix_param=0.1,
    adjust_scf='never',
    compute_forces=forces,
    species_dir=os.environ["AIMS_SPECIES_DIR"] + f'/../../{species_set}/',
    density_update_method="density_matrix",
    tier=[elem_tiers[elem] for elem in dict.fromkeys(asi.atoms.symbols)] # dict() preserves order: 
                                                                     # https://gitlab.com/ase/ase/-/commit/7b70eddd026154d636faf404cc2f8c7b08d89667
                                                                     # https://mail.python.org/pipermail/python-dev/2017-December/151283.html
  )
  calc.write_input(asi.atoms)
