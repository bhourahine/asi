import numpy as np

def dm_norms(dms, n_mol):
  n = dms.shape[-1]
  assert dms.shape[-2] == n
  assert n % n_mol == 0
  total_norm = np.linalg.norm(dms, axis=(-2,-1))
  dms_blocks = dms.reshape((-1, n_mol, n//n_mol, n_mol, n//n_mol))
  diag_norm = np.linalg.norm([np.linalg.norm(dms_blocks[:, i, :, i, :], axis=(-2,-1)) for i in range(n_mol)], axis=0)
  offdiag_norm = (total_norm**2 - diag_norm**2)**0.5
  return total_norm / (n_mol**0.5), diag_norm / (n_mol**0.5), offdiag_norm / (n_mol**0.5)
  
