import sys, os
import numpy as np
from pathlib import Path
from ase import Atoms
from ase.data import chemical_symbols
from asi4py.asecalc import ASI_ASE_calculator
from asi4py.dm import save_atomic_dm
from multiprocessing import Process, Queue
from init_asi import init_via_ase, elem_tiers, species_set

def get_dm(return_queue, elem, initializer):
  atoms = Atoms(elem)
  calc = ASI_ASE_calculator(os.environ['ASI_LIB_PATH'], initializer, None, atoms)
  calc.asi.keep_density_matrix='history' # save all SCF iterations
  calc.asi.keep_overlap=True

  atoms.calc = calc
  print (atoms.numbers[0], elem, calc.asi.n_basis, species_set, f'{atoms.get_potential_energy():.6f}', len(calc.asi.dm_storage[(1,1)]))
  dm=np.array(calc.asi.dm_storage[(1,1)])
  print (dm.shape)
  S = calc.asi.overlap_storage[(1,1)]
  return_queue.put([dm, S])

if __name__ == '__main__':
  os.makedirs(species_set, exist_ok=True)

  return_queue = Queue()
  for elem, tier in elem_tiers.items():
    elem_Z = chemical_symbols.index(elem)
    print (elem_Z, elem, tier)
    p = Process(target=get_dm, args=(return_queue, elem, init_via_ase))
    p.start()
    dm, S = return_queue.get()
    p.join()
    print ("Nel=", np.sum(dm[-1] * S))
    save_atomic_dm(dm[-1], species_set, elem, tier)
    dmerr = dm[:, ...] - dm[-1, ...]
    print ("DM errors:")
    np.savetxt(sys.stdout, np.linalg.norm(dmerr, axis=(1,2)), fmt='%15.8f')




