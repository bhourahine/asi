import sys, os
import numpy as np
from pathlib import Path
from ase import Atoms
from ase.io import read
from asi4py.asecalc import ASI_ASE_calculator
from init_asi import init_via_ase, elem_tiers, species_set


'''

Usage example:

python3 -u save_mol_dm.py CH4_clath/structures/cluster/ch4.xyz CH4_clath/structures/cluster/ch4.npz

'''

mol = read(sys.argv[1])
dm_out_fn = sys.argv[2]
assert Path(dm_out_fn).suffix.lower() == '.npz'
mol.calc = ASI_ASE_calculator(os.environ['ASI_LIB_PATH'], init_via_ase, None, mol)
mol.calc.asi.keep_density_matrix = True
E = mol.get_potential_energy()
print (f'E = {E:.6f}')
dm=np.array(mol.calc.asi.dm_storage[(1,1)])
np.savez_compressed(dm_out_fn, dm=dm)

