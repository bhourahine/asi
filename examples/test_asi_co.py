import sys, os, io
from glob import glob
import numpy as np
from ctypes import cast, py_object
from mpi4py import MPI
from ase.io import read
from asi4py.asecalc import ASI_ASE_calculator
from ase.build import bulk
from ase.parallel import parprint


def print_to_string(*args, **kwargs):
  # inspired by https://stackoverflow.com/a/39823534
  with io.StringIO() as output:
    print(*args, file=output, **kwargs)
    return output.getvalue()

def parprint(*args, **kwargs):
  if MPI.COMM_WORLD.Get_rank() == 0:
    print (*args, **kwargs)

def ordprint(*args):
  """
    Print from all processes in rank order
  """
  s = print_to_string(*args)
  strings = MPI.COMM_WORLD.gather(s)
  if MPI.COMM_WORLD.Get_rank() == 0:
    for s in strings:
      print(s, end="")


ASI_LIB_PATH = os.environ["ASI_LIB_PATH"]

def init_via_ase(asi):
  from ase.calculators.aims import Aims
  calc = Aims(xc='PBE', 
    spin='collinear',
    occupation_type="gaussian 0.01",
    mixer="pulay",
    k_grid=(3,2,2),
    n_max_pulay=10,
    charge_mix_param=0.1,
    sc_accuracy_rho=1E-05,
    sc_accuracy_eev=1E-03,
    sc_accuracy_etot=1E-06,
    #sc_accuracy_forces=1E-04,
    sc_iter_limit=100,
    compute_forces=False,
    density_update_method='density_matrix', # for DM export
  )
  calc.write_input(asi.atoms)

def overlap_callback(aux, iK, iS, descr, data, matrix_descr_ptr):
  try:
    asi = cast(aux, py_object).value
    data_shape = (asi.n_basis,asi.n_basis) if asi.is_hamiltonian_real else (asi.n_basis,asi.n_basis, 2)
    data = asi.scalapack.gather_numpy(descr, data, data_shape)
    is_descr = True if descr else False
    log_data = f"{data.shape} {np.sum(data)} {is_descr}" if data is not None else "NOTHING"
    #print ("S:", MPI.COMM_WORLD.rank, iK, iS, log_data)
    atoms.calc.asi.S[(iK,iS)] = data.copy()
  except Exception as eee:
    print (f"Something happened in ASI overlap_callback: {eee}\nAborting...")
    MPI.COMM_WORLD.Abort(1)

def dm_callback(aux, iK, iS, descr, data, matrix_descr_ptr):
  try:
    asi = cast(aux, py_object).value
    data_shape = (asi.n_basis,asi.n_basis) if asi.is_hamiltonian_real else (asi.n_basis,asi.n_basis, 2)
    data = asi.scalapack.gather_numpy(descr, data, data_shape)
    is_descr = True if descr else False
    #nel = np.sum(asi.S[(iK,iS)] * np.conj(data))
    nel = np.einsum('ij,ij,ki->k',np.conj(data), asi.S[(iK,iS)], asi.atomic_prj)
    log_data = f"{data.shape} {np.sum(data)} {is_descr} {nel}" if data is not None else "NOTHING"
    if (iK,iS) not in asi.nel:
      asi.nel[(iK,iS)] = []
    asi.nel[(iK,iS)].append(nel)

    #ordprint ("D:", MPI.COMM_WORLD.rank, iK, iS, log_data)
  except Exception as eee:
    print (f"Something happened in ASI overlap_callback: {eee}\nAborting...")
    MPI.COMM_WORLD.Abort(1)


atoms = bulk('Co')
atoms.set_initial_magnetic_moments([1, 1])
atoms.calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, MPI.COMM_WORLD, atoms, work_dir="./", logfile="aims.out")

atoms.calc.asi.S = {}
atoms.calc.asi.nel = {}

parprint (f'atoms.calc.asi.basis_atoms={atoms.calc.asi.basis_atoms}')

atoms.calc.asi.atomic_prj = np.zeros((len(atoms), atoms.calc.asi.n_basis))
atoms.calc.asi.atomic_prj[atoms.calc.asi.basis_atoms, range(atoms.calc.asi.n_basis)] = 1
#atoms.calc.asi.atomic_prj = 1.0

atoms.calc.asi.register_overlap_callback(overlap_callback, atoms.calc.asi)
atoms.calc.asi.register_dm_callback(dm_callback, atoms.calc.asi)

E = atoms.get_potential_energy()
parprint (f'E={E}')

ordprint(MPI.COMM_WORLD.rank, atoms.calc.asi.nel)

nel = MPI.COMM_WORLD.gather(atoms.calc.asi.nel, root=0)
if MPI.COMM_WORLD.rank == 0:
  nel = {k: v for d in nel for k, v in d.items()}
  nel = np.array([[nel[(k,s)] for s in range(1,atoms.calc.asi.n_spin+1)] for k in range(1,atoms.calc.asi.n_kpts+1)])
  np.savez("nel.npz", nel=nel)
  

