from ctypes import cdll, CDLL, RTLD_GLOBAL
from ctypes import POINTER, byref, c_int, c_int64, c_int32, c_bool, c_char_p, c_double, c_void_p, CFUNCTYPE, py_object, cast, byref
import ctypes


import sys, os
import numpy as np
from pathlib import Path
from ase import Atoms
from ase.data import chemical_symbols
from ase.build import molecule
from asi4py.asecalc import ASI_ASE_calculator


def init_via_ase(asi, tier):
  from ase.calculators.aims import Aims
  calc = Aims(xc='pbe', 
    relativistic='atomic_zora scalar',
    #relativistic='none',
    #override_relativity=True,
    density_update_method="density_matrix",
    sc_iter_limit=300,
    n_max_pulay=8,
    charge_mix_param=0.1,
    adjust_scf='never',
    tier=None if tier == 'default' else tier
  )
  calc.write_input(asi.atoms)


def default_saving_callback(aux, iK, iS, descr, data):
  try:
    asi = cast(aux, py_object).value
    data_shape = (asi.n_basis,asi.n_basis) if asi.is_hamiltonian_real else (asi.n_basis,asi.n_basis, 2)
    data = asi.scalapack.gather_numpy(descr, data, data_shape)
    if data is not None:
      assert len(data.shape) == 2
      if (iK, iS) not in asi.dm_storage:
        asi.dm_storage[(iK, iS)] = []
      asi.dm_storage[(iK, iS)].append(data.copy())
  except Exception as eee:
    print (f"Something happened in ASI default_saving_callback: {eee}\nAborting...")
    MPI.COMM_WORLD.Abort(1)

def atomic_dm(atoms, tier):
  elems = get_elems_in_order(atoms)
  assert len(elems) == len(tier)
  numbers = [chemical_symbols.index(e) for e in elems]
  species_dir = os.environ["AIMS_SPECIES_DIR"] # 
  species_path = Path(species_dir)
  species_tag = '/'.join(species_path.parts[-2:])
  dms = [np.load(f'{species_tag}/{num}_{elem}_{t}.npz')['dm'] for elem,num,t in zip (elems, numbers, tier)]
  elems_indices = [elems.index(s) for s in atoms.symbols]
  n = sum([dms[i].shape[0] for i in elems_indices])
  dm = np.zeros((n,n))
  j = 0
  for i in elems_indices:
    m = dms[i].shape[0]
    dm[j:j+m, j:j+m] = dms[i]
    j += m
  assert j == n, (n,j)
  
  return dm

  

def get_elems_in_order(atoms):
  elems = []
  for s in atoms.symbols:
    if s not in elems:
      elems.append(s)
  return elems

atoms = molecule(sys.argv[1])
tier = list(map(int, sys.argv[2:]))

dm_init = atomic_dm(atoms, tier)


calc = ASI_ASE_calculator(os.environ['ASI_LIB_PATH'], lambda x: init_via_ase(x, tier), None, atoms)

calc.asi.dm_storage = {}
calc.asi.register_dm_callback(default_saving_callback, calc.asi)
if True:
  calc.asi.init_density_matrix = {(1,1):dm_init}

atoms.calc = calc
print (calc.asi.n_basis, f'{atoms.get_potential_energy():.3f}', len(calc.asi.dm_storage[(1,1)]))
dm=np.array(calc.asi.dm_storage[(1,1)])
print (dm.shape)
dmerr = dm[:, ...] - dm[-1, ...]
np.savetxt(sys.stdout, np.linalg.norm(dmerr, axis=(1,2)), fmt='%15.8f')

print("dm_init err", np.linalg.norm( dm_init - dm[-1, ...]))



