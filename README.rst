Atomic Simulation Interface (ASI) API
=====================================

The Atomic Simulation Interface (ASI) is a native C-style API that includes
functions for the export and import of data structures that are used in
electronic structure calculations and for classical molecular dynamics
simulations. ASI aims to be a uniform, generic and efficient interface
for connecting various computational chemistry and materials science
codes in multiscale simulation workflows, such as QM/MM, QM/ML, QM/QM.
ASI specifies functions, data types and calling conventions for export
and import of density matrices, overlap and Hamiltonian matrices,
electrostatic potentials, atomic coordinates, charges, total energy and
forces.

`Full ASI API documentation <https://pvst.gitlab.io/asi>`__.

ASI API specification
---------------------

The ASI API is specified as a C header file `asi.h`_. Codes using ASI API must provide a linkable library with definitions of functions matching `asi.h`_. Depending on particular usage of the implementations, some functions can be omitted or implemented as stubs, if they are not going to used. To use the Python ASI wrapper it is necessary to have all functions from `asi.h` defined, but of course stub definitions can be used.

.. _`asi.h`: https://pvst.gitlab.io/asi/asi_8h.html

`Doxygen-generated ASI API
specification <https://pvst.gitlab.io/asi/asi_8h.html>`__.

Implementations
---------------

-  `DFTB+ <https://dftbplus.org/>`__: `in separate
   branch <https://github.com/PavelStishenko/dftbplus/tree/ASI_v1.3>`__.
   Milestone release: ``24.2``. ASI API is provided by a separate
   library that should be linked with the DFTB+ library.
-  `FHI-aims <https://fhi-aims.org/>`__: in the main branch.

Supported platforms
-------------------

FHI-aims
~~~~~~~~

The ASI API implementation in FHI-aims is routinely tested on Linux machines
with the following compilers and libraries:

-  Linux, GNU 7.5.0, Intel MKL 2021.4, and Intel MPICH 3.2.1.
-  Linux, Intel compilers, Intel MKL 2021.4, and Intel MPICH 3.2.1.
-  Debian Stable, GNU 9.3, OpenBLAS 0.3, and OpenMPI 4.1

DFTB+
~~~~~

Although the DFTB+ works on both Linux and MacOS machines, the ASI API
implementation is not tested for MacOS and build scripts are Linux-only.

The well-tested configurations are:

-  Debian Stable with GCC 9.3 and OpenMPI 4.1
-  Fedora 34 with GCC 11.3 and OpenMPI 4.1
-  Debian Testing with GCC 12.2 and OpenMPI 4.1

Python wrapper
--------------

ASI-enabled codes can be used in Python scripts via the ``asi4py``
wrapper of the ASI API.

To install asi4py use ``pip``

::

   pip install asi4py

See `asi4py reference in documentation <https://pvst.gitlab.io/asi/reference.html>`_.

Usage example
==================================

See tests and their `descriptions <https://pvst.gitlab.io/asi/testing.html#tests-descriptions>`_ in the documentation for various usage scenarios.

A simple asi4py example
------------------------

.. code-block:: python

  import sys, os
  import numpy as np
  from ctypes import CDLL, RTLD_GLOBAL

  from ase.build import molecule
  from ase.io import read, write
  from asi4py.asecalc import ASI_ASE_calculator
  from ase import units

  # read path to ASI-implementing shared library from environment variable
  ASI_LIB_PATH = os.environ['ASI_LIB_PATH']
  # load the ASI lib for direct C API calls, 
  # RTLD_GLOBAL is necessary for MPI
  asilib = CDLL(ASI_LIB_PATH, mode=RTLD_GLOBAL)

  # use direct ASI API call to distinguish ASI implementations
  # ASI_flavour()==1 for FHI-aims; ASI_flavour()==2 for DFTB+
  # See documentation for details: https://pvst.gitlab.io/asi/asi_8h.html#a3db29227112f71d020732b18db023315
  if asilib.ASI_flavour() == 1:
    # FHI-aims initializer via ase.calculators.aims.Aims calculator
    def init_via_ase(asi):
      from ase.calculators.aims import Aims
      calc = Aims(xc='pbe', 
        relativistic="atomic_zora scalar",
        occupation_type="gaussian 0.010",
        density_update_method="density_matrix"
      )
      calc.write_input(asi.atoms)
  else:
    # DFTB+ initializer via ase.calculators.dftb.Dftb calculator
    def init_via_ase(asi):
      from ase.calculators.dftb import Dftb
      calc = Dftb(label='Some_cluster',
            Hamiltonian_SCC='Yes',
            Hamiltonian_MaxAngularMomentum_='',
            Hamiltonian_MaxAngularMomentum_O='"p"',
            Hamiltonian_MaxAngularMomentum_H='"s"')
      calc.write_input(asi.atoms, properties=['forces'])

  atoms = molecule('H2O')

  # initialize ASI library via ASE calculators 
  atoms.calc = ASI_ASE_calculator(ASI_LIB_PATH, init_via_ase, None, atoms)

  # Ask to save density, Hamiltonian, and overlap matrices
  # See documentation for details: https://pvst.gitlab.io/asi/reference.html#asi4py.pyasi.ASIlib.keep_density_matrix
  atoms.calc.asi.keep_density_matrix = True
  atoms.calc.asi.keep_hamiltonian = True
  atoms.calc.asi.keep_overlap = True

  # Get number of basis functions via asi4py wrapper: https://pvst.gitlab.io/asi/reference.html#asi4py.pyasi.ASIlib.n_basis
  print(f'basis size = {atoms.calc.asi.n_basis}')

  # Do actual calculations (ASI_ASE_calculator calls ASI_run() underneath)
  print(f'E = {atoms.get_potential_energy():.6f}')

  # Get saved matrices. Index (1,1) means 1st k-point and 1st spin channel 
  # See documentation for details: https://pvst.gitlab.io/asi/reference.html#asi4py.pyasi.ASIlib.keep_density_matrix
  S = atoms.calc.asi.overlap_storage[(1,1)]
  H = atoms.calc.asi.hamiltonian_storage[(1,1)]
  DM = atoms.calc.asi.dm_storage.get((1,1), None)

  # Do simple arithmetic on matrices to get electronic structure quantities
  print(f'Total number of electrons = {np.sum(S*DM.T):.6f}')
  print(f'Sum of eigenvalues  = {np.sum(H*DM.T):.6f}')

