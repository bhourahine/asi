=====================================
Building
=====================================


.. _fhi-aims-1:

FHI-aims
=====================================

FHI-aims has embedded support of ASI API. Just build the latest version of
FHI-aims as a shared library and use with your code.

.. _dftb-1:

DFTB+
=====================================

#. Download DFTB+ source from `the branch with ASI
   API support <https://github.com/PavelStishenko/dftbplus/archive/refs/heads/ASI_v1.3.zip>`__
   or use git to clone it:: 
   
     git clone -b ASI_v1.3 --single-branch https://github.com/PavelStishenko/dftbplus.git
   
#. Follow `DFTB+ building instructions <https://github.com/PavelStishenko/dftbplus/blob/ASI_v1.3/INSTALL.rst>`__ to build DFTB+ with as a shared libraries. 
   Set CMake flags ``BUILD_SHARED_LIBS``, ``WITH_API``, and ``ENABLE_DYNAMIC_LOADING`` to ``TRUE`` for that. Also consider enabling MPI support with the flag ``WITH_MPI``, select proper compilers,  options for compilers and linker via ``CC``, ``FC``, ``CFLAGS``, ``FFLAGS``, and ``LDFLAGS`` environment variables, and set other relevant CMake variables (``SCALAPACK_LIBRARY``, ``LAPACK_LIBRARY``, etc). 
   For example (with MPI)::

     FC=mpif90 CC=mpicc LDFLAGS="-Wl,--copy-dt-needed-entries" cmake -DCMAKE_INSTALL_PREFIX=$HOME/opt/dftb+ -DBUILD_SHARED_LIBS=TRUE -DWITH_API=TRUE -DENABLE_DYNAMIC_LOADING=TRUE -DWITH_MPI=TRUE -DLAPACK_LIBRARY=openblas -DSCALAPACK_LIBRARY=scalapack-openmpi -B _build .
     cmake --build _build
     cmake --install _build
    
   Another example (without MPI)::
     FC=gfortran CC=gcc FFLAGS="-w" LDFLAGS="-Wl,--copy-dt-needed-entries"  cmake  -DLAPACK_LIBRARY=openblas -DCMAKE_INSTALL_PREFIX=${PWD}/install -DENABLE_DYNAMIC_LOADING=true -DBUILD_SHARED_LIBS=true -DWITH_UNIT_TESTS=true -DCMAKE_BUILD_TYPE=Debug -B _build-nompi -C nompi.config.cmake .
   
   Note, that some variables such as ``MKL_ROOT`` may affect CMake's libraries search outcome, so you may want to unset them. Also if you have multiple MPI or BLAS implementations, consider setting the ``CMAKE_PREFIX_PATH`` variable. For example::
   
     unset MKL_ROOT
     CMAKE_PREFIX_PATH=/usr/lib64/openmpi FC=mpif90 CC=mpicc LDFLAGS="-Wl,--copy-dt-needed-entries" cmake -DCMAKE_INSTALL_PREFIX=$HOME/opt/dftb+ -DBUILD_SHARED_LIBS=TRUE -DWITH_API=TRUE -DENABLE_DYNAMIC_LOADING=TRUE -DWITH_MPI=TRUE -DLAPACK_LIBRARY=openblas -DSCALAPACK_LIBRARY=scalapack -B _build .
   

#. Set environment variables ``DFTBP_INCLUDE`` and ``DFTBP_LIB_DIR`` to
   folders with DFTB+ C-headers and libraries.

#. Set environment variables ``INSTALL_PREFIX`` and optionally
   ``BUILD_PATH`` to set installation and building locations.

#. From the root of the working copy of the ASI repository run ``make install``. The Makefile uses ``mpicxx`` command. So make sure that is available in ``PATH``. For example::

     PATH=$PATH:/lib64/openmpi/bin DFTBP_INCLUDE=~/opt/dftb+/include/ DFTBP_LIB_DIR=~/opt/dftb+/lib/ INSTALL_PREFIX=$PWD/install make install

#. The shared library implementing ASI API for DFTB+ will be instaled in
   ``${INSTALL_PREFIX}/lib``, the header file ``asi.h`` will be instaled in
   ``${INSTALL_PREFIX}/include``.


