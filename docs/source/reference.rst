asi4py reference
==================================================================================================

Python wrapper for ASI-enabled libraries
----------------------------------------------

.. automodule:: asi4py.pyasi
   :members:
   :special-members: __init__,__call__

ASI ASE claculator
-----------------------------

.. automodule:: asi4py.asecalc
   :members:

