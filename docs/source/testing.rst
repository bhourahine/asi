========
Testing
========

The folder ``tests`` contains files for testing ASI implementaions for FHI-aims and DFTB+.

The folders ``tests/include`` and ``tests/src``  contains source codes for native ASI tests (written in C++).

The folder ``tests/python`` contains source codes for Python ASI tests (use ``asi4py`` wrapper).

The folder ``tests/testcases`` contains scripts for launching tests for DFTB+ and FHI-aims ASI implementations.

Build tests
=================

To build tests set the following environment variables:

#. ``BUILD_PATH`` path where the compiled tests will be placed (``tests/build/`` by default)
#. ``ASI_LIB_PATH`` path to the ASI-implementing library (``libaims.so`` or ``libasidftbp.so``)
#. ``ASI_INCLUDE_DIR`` path to the folder with ``asi.h`` header file.
#. optionally ``DFTBP_LIB_DIR`` path to the folders with DFTB+ libraries (``libdftbplus.so``, ``libmpifx.so``, etc)
#. optionally ``AIMS_LIB_DIR`` path to the folders with FHI-aims libraries (``libaims.so``, ``libaims1.so``, ``libaims2.so``, etc)
#. common ``make`` variables, such as ``CC``, ``CFLAGS``, and ``LDFLAGS``

To build tests for FHI-aims run ``make aims_tests`` in ``tests/`` directory.

To build tests for DFTB+ run ``make aims_dftbp`` in ``tests/`` directory.

For example, building tests for FHI-aims::

   CC=mpicxx ASI_INCLUDE_DIR=../include/ ASI_LIB_PATH=$HOME/opt/aims/lib/libaims.so AIMS_LIB_DIR=$HOME/opt/aims/lib/  make aims_tests

For example, building tests for DFTB+::

   CC=mpicxx ASI_INCLUDE_DIR=../include/ ASI_LIB_PATH=$PWD/../install/lib/libasidftbp.so DFTBP_LIB_DIR=~/opt/dftb+/lib/  make dftbp_tests

Run tests
===============


To run tests set the following environment variables:

#. ``ASI_LIB_PATH`` path to the ASI-implementing library (``libaims.so`` or ``libasidftbp.so``)
#. ``TESTS`` path to the compiled tests (``tests/build/`` by default)
#. ``PYTESTS`` path to tests of Python ASI wrapper (``tests/python/`` by default)
#. to the ``LD_LIBRARY_PATH`` variable add paths of the folders with FHI-aims or DFTB+ libraries (one may reuse ``DFTBP_LIB_DIR`` and ``AIMS_LIB_DIR`` variables from the build stage).
#. ``TESTING_PYTHON`` path to the Python interpreter of the environment that has ``asi4py`` installed (``python3`` by default). To install ``asi4py`` use ``pip``: ``pip install asi4py``.
#. ``MPIEXEC`` to ``mpiexec`` command, such as ``mpiexec.openmpi`` or ``/lib64/openmpi/bin/mpiexec`` (``mpiexec`` by default).
#. optionally ``AIMS_SPECIES_DIR`` path to FHI-aims directory with species default basis sets (``defaults_2010/tight`` is expected by tests)
#. optionally ``DFTB_PREFIX`` path to directory with Slater-Koster files (``*.skf``) for DFTB+. These files can be downloaded into the working copy of the DFTB+ repository running command: ``./utils/get_opt_externals``. The files will be in the ``external/slakos/origin/`` directory.
#. for DFTB+ tests set ``OMP_NUM_THREADS=1``

Then go to the ``tests/testcases`` directory and run ``./run_tests test_*.aims`` to run FHI-aims 

For example running FHI-aims tests::
  
  AIMS_SPECIES_DIR=$HOME/opt/aims/species_defaults/defaults_2010/tight/ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/opt/aims/lib/ ASI_LIB_PATH=$HOME/opt/aims/lib/libaims.so TESTING_PYTHON=$HOME/ve311/bin/python ./run_tests.sh test_*aims


For example running DFTB+ tests::

  OMP_NUM_THREADS=1 DFTB_PREFIX=$HOME/opt/dftb+/slakos/origin/mio-1-1/ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/opt/dftb+/lib/ ASI_LIB_PATH=$PWD/../../install/lib/libasidftbp.so TESTING_PYTHON=$HOME/ve311/bin/python ./run_tests.sh test_*dftbp
  
Tests descriptions
=====================

C++ tests
---------

#. **test_min.cpp** - minimal example. Getting basis set size, and energy calculation with MPI.
#. **test_esp.cpp** - import electrostatic potential via `ASI_register_external_potential <https://pvst.gitlab.io/asi/asi_8h.html#a8a128aab70666e822f86792df1b7750c>`_ or `ASI_set_external_potential functions <https://pvst.gitlab.io/asi/asi_8h.html#a9a0df650805163523f19c2c62955f5ac>`_. Two potentials are provided: homogeneous and pointwise.
#. **test_dm_min.cpp** - minimal showcase for density matrix export via `ASI_register_dm_callback <https://pvst.gitlab.io/asi/asi_8h.html#a8e8308492ad5f15c152a1cb6c21eea6b>`_. Distributed calculations are not supported for the sake of clarity.
#. **test_initdm.cpp** - initialize SCF loop using loaded density matrix. Showcase for `ASI_register_dm_init_callback <https://pvst.gitlab.io/asi/asi_8h.html#afed2ba0037cf22d7da10d2aa106efe9c>`_, and some auxilary functions :``ASI_get_nkpts``, ``ASI_is_hamiltonian_real``, ``ASI_get_nspin``. Distributed calculations are supported. BLACS subroutines are used to distribute initializing density matrix (see auxilary functions in ``tests/include/blacsutils.h``).
#. **test_expdmhs.cpp** - export overlap, hamiltonian, and density matrices via `ASI_register_overlap_callback  <https://pvst.gitlab.io/asi/asi_8h.html#a29c4b96ebe3f9496da387dda98060740>`_, `ASI_register_hamiltonian_callback <https://pvst.gitlab.io/asi/asi_8h.html#ae770f0aef1e869997d304c157f56cebf>`_, and `ASI_register_dm_callback <https://pvst.gitlab.io/asi/asi_8h.html#a8e8308492ad5f15c152a1cb6c21eea6b>`_. Distributed calculations are supported. BLACS subroutines are used to gather matrices (see auxilary functions in ``tests/include/blacsutils.h``).

Python tests
-------------

#. **test_e.py** - minimal example. Simple energy calculations via :class:`~asi4py.asecalc.ASI_ASE_calculator` wrapper.
#. **test_pbc.py** - showcase for a syste with periodic boundaries. Stress tensor and forces are calculated.
#. **test_esp.py** - import external pointwise electrostatic potential via asi4py :func:`~asi4py.pyasi.ASIlib.register_external_potential`, read :attr:`~asi4py.pyasi.ASIlib.atomic_charges`
#. **test_sc.py** - self-consistent electrostatic coupling of two ASI calculations via :func:`~asi4py.pyasi.ASIlib.register_external_potential` and :func:`~asi4py.pyasi.ASIlib.calc_esp`. The :class:`~asi4py.pyasi.ASIlib` is used directly, without :class:`~asi4py.asecalc.ASI_ASE_calculator` wrapper for proper control of ASI libraries lifetimes.
#. **test_expdmhs.py** - showcase of low-level access to matrix export callbacks via asi4py: :func:`~asi4py.pyasi.ASIlib.register_overlap_callback`, :func:`~asi4py.pyasi.ASIlib.register_dm_callback`, and :func:`~asi4py.pyasi.ASIlib.register_hamiltonian_callback`.
#. **test_expdmhs_default.py** - showcase of a simplified way to export overlap, density, and Hamiltonian matrices via flags: :attr:`~asi4py.pyasi.ASIlib.keep_overlap`, :attr:`~asi4py.pyasi.ASIlib.keep_density_matrix`, and :attr:`~asi4py.pyasi.ASIlib.keep_hamiltonian`.
#. **test_expdmhs_esp.py** - mixture of **test_expdmhs.py** and  **test_esp.py**.



