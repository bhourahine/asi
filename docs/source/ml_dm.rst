
Integration with Density Matrix ML models
---------------------------------------------------------------------------------------

ASI API provides a few functions that are especially valuable for integration with 
electronic structure machine learning (ESML) models, such as SchNOrb, ACEhamiltonians,
DeepH, DeepH-D3, *etc*. Those functions are ``ASI_register_dm_init_callback`` and ``ASI_register_set_hamiltonian_callback``.
Using these functions one can initialize the SCF loop of DFT calculations by the density or Hamiltonian matrix, that
is predicted by some ESML model, and is a much more accurate guess of the ground state electronic structure. Therefore the
number of SCF loop iterations before convergence can be reduced.

Both ``ASI_register_dm_init_callback`` and ``ASI_register_set_hamiltonian_callback`` functions
are implemented in ``DFTB+``, and only the first one (for the density matrix) is supported by ``FHI-aims``. 
Nevertheless, it is straightforward to compute density matrix from Hamiltonian and overlap matrices and use 
it for SCF initialization, even if the ESML model on hands predicts only the Hamiltonian matrix. 
The utility function :func:`asi4py.pyasi.HS_to_DM` can do it for you.

The :mod:`asi4py.dm` module provides a few utility classes and functions for advanced density matrix intialization.
The :class:`PredictDMByAtoms <asi4py.dm.PredictDMByAtoms>` class is a base class for density matrix predictors, that
use atomic positions as an argument. Here are few usage examples of :class:`PredictDMByAtoms <asi4py.dm.PredictDMByAtoms>` derived classes.

Compute and save density matrix of a free atom
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here is a simple example of module :mod:`asi4py.dm` usage. This script uses `FHI-aims` code 
to compute a single-atom density matrix and saves it into the directory specified as the
``ASI_FREE_ATOM_DMS`` environment variable using function :func:`asi4py.dm.save_atomic_dm`.


.. literalinclude:: ../../tests/python/test_save_dm.py
    :language: python

Make sure that environment variables ``ASI_LIB_PATH`` and ``AIMS_SPECIES_DIR`` are configured correctly and launch the script above as:

.. code-block:: bash

   ASI_FREE_ATOM_DMS=/tmp python3 test_save_dm.py H

The output should be:

.. code-block:: console

   Saving H DM in basis from /aims/species_defaults/defaults_2020/light into /tmp directory
   E = -12.483009

Repeat it for other elements you are going to work with:

.. code-block:: bash

   ASI_FREE_ATOM_DMS=/tmp python3 test_save_dm.py O
   ASI_FREE_ATOM_DMS=/tmp python3 test_save_dm.py C
   
The following files will appear in ``/tmp``:

.. code-block:: console

    /tmp/001_H.ncdf
    /tmp/006_C.ncdf
    /tmp/008_O.ncdf

`NetCDF` format is used to save and load these files via :class:`scipy.io.netcdf_file` class.


Free-atom density matrix initialization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :class:`asi4py.dm.PredictFreeAtoms` uses atomic density matrices from the ``ASI_FREE_ATOM_DMS`` directory 
to guess the initial density matrix for SCF loop start. This is a common SCF initialization algorithm used by many 
DFT codes. Therefore it doesn't give any benefits on its own. But it is usefull in combinations with other DM predictors (see examples below). 
Here we use it as a simple example of a DM predictor derived from :class:`asi4py.dm.PredictDMByAtoms`. This example
prints number of SCF steps from the :attr:`asi4py.pyasi.ASILib.dm_calc_cnt` dictionary that tracks number of calls of the defaul callback 
registered by the :attr:`asi4py.pyasi.ASILib.keep_density_matrix` attribute.
The ground state density matrix is saved using :func:`numpy.savez` in the current directory.

Note how the method :meth:`asi4py.dm.PredictDMByAtoms.register_DM_init` is used to configure the predictor to be invoked
on SCF loop intialization.

.. literalinclude:: ../../tests/python/test_free_atom_dm_init.py
    :language: python

Launch the script above as:

.. code-block:: bash

   ASI_FREE_ATOM_DMS=/tmp python3 test_free_atom_dm_init.py CH4

The output should be:

.. code-block:: console

   E = -1101.826436
   Number of SCF steps: 11


Density matrix initialization with a precomputed matrix
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following example uses :class:`asi4py.dm.PredictConstAtoms` class to initialize SCF loop with a density matrix
loaded from a file in the current directory using  :func:`numpy.load` function. The same result can be achieved 
in a simpler way by setting the :attr:`asi4py.ASILib.init_density_matrix` property. But this class is usefull in
combinations with other DM predictors (see examples below). 

This example uses :func:`ase.Atoms.rattle` to disturb the geometry
of the created molecule, so it doesn't exactly match the loaded density matrix. Yet while the ``stdev``
parameter of the :func:`ase.Atoms.rattle` function is not too large, the number of SCF steps is still smaller than in
the previous example for the same molecul. That is because the density matrix of a molecule with a close yet different 
geometry is a better initial guess for SCF loop than the free-atomic initialization.

.. literalinclude:: ../../tests/python/test_const_dm_init.py
    :language: python

This script expects that an ``*.npz`` file exists in the current directory. Such files are created by the previous example.
Launch the script as following:

.. code-block:: bash

   ASI_FREE_ATOM_DMS=/tmp python3 test_free_atom_dm_init.py CH4

The output should be:

.. code-block:: console

   E = -1101.826466
   Number of SCF steps: 6

Try to change the the ``stdev`` parameter of the :func:`ase.Atoms.rattle` function by the decimal order of magnitude to 
see how geometry difference affects number of SCF steps for convergence.


"Stitching" density matrices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following example uses the class :class:`asi4py.dm.PredictFrankensteinDM` to construct a density matrix of 
a complex of a :math:`CO_2` molecule and a copper atom. Constructor of :class:`asi4py.dm.PredictFrankensteinDM` 
takes as argument a list of pairs, where the first element is some object of a class derived from
:class:`asi4py.dm.PredictDMByAtoms`, and the second element is a list of atomic indices. Each pair in the list
describes some part of the system. Atomic indices must cover all the system, with possible overlaps. For
overlapping parts, the DM elements are simply averaged over multiple predictions. This approach works only
for atomic-centered basis sets, like in FHI-aims or DFTB+. In this example we use :class:`asi4py.dm.PredictConstAtoms`
for the :math:`CO_2` molecule DM, and an instance of :class:`asi4py.dm.PredictFreeAtoms` for the Cu atom.

.. literalinclude:: ../../tests/python/test_frankenstein_dm_init.py
    :language: python


This script expects existence file with free-atomic DM (for Cu) and molecular DM (for :math:`CO_2`). Therefore 
before launching this script, we need to run a few previous examples with specific arguments:

.. code-block:: bash

   # make files with free-atomic DMs in  /tmp:
   ASI_FREE_ATOM_DMS=/tmp python3 test_save_dm.py C
   ASI_FREE_ATOM_DMS=/tmp python3 test_save_dm.py O
   ASI_FREE_ATOM_DMS=/tmp python3 test_save_dm.py Cu

   # make CO2 DM in current directory:
   ASI_FREE_ATOM_DMS=/tmp python3 test_free_atom_dm_init.py CO2
   
   # Finally compute CO_2-Cu complex:
   python3 test_frankenstein_dm_init.py

The output of the last command should be:

.. code-block:: console

   E = -50376.414918
   Number of SCF steps: 14

Note that SCF loop converged in 14 steps. To compared it with the default convergence speed uncomment the 
line ``mol_dm_predictor = PredictFreeAtoms()`` and comment-out the previous line ``mol_dm_predictor = PredictConstAtoms(mol, mol_DM)``:


In that case the script launch will look like that:

.. code-block:: console

   $ python3 test_frankenstein_dm_init.py
   E = -50376.414918
   Number of SCF steps: 18

Therefore the stitched DM is 4 SCF steps closer to the ground-state DM than the default free-atomic guess.



Extrapolating density matrices using Kernel Ridge Regression
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This example shows how to use the :class:`asi4py.dm.PredictSeqDM` predictor, that uses Kernel Ridge Regression 
to learn density matrix on-the-fly. It can be considered as `extrapolation` or `active learning` technique.
The :class:`asi4py.dm.PredictSeqDM` predictor uses some other predictor as a baseline, and learns only the difference 
between baseline and ground-state DM. In this example the :class:`asi4py.dm.PredictFreeAtoms` predictor is used as a baseline.
Usage of the baseline predictor solves the problem of different scales of DM elements, numerically stabilizing the regression.
The function :func:`asi4py.dm.PredictSeqDM.update_errs` should be used to provide training samples to the predictor. 
The :func:`update_errs` function must be called once after each prediction.

In this example the LBFGS algorithm is used for local relaxation of a molecule bult by the :func:`ase.build.molecule` function.
To provide the training sample to the predictor the callback function :func:`dyn_step` is registered to be invoked 
on each geometry step. Note that DM predictor must be re-registerd on each step by the 
:func:`asi4py.dm.PredictDMByAtoms.register_DM_init` function, because the DM intialization callback is resetted on each step.
The :attr:`asi4py.pyasi.ASILib.keep_density_matrix` attribute is also turned on each geometry step to reset 
the :attr:`asi4py.pyasi.ASILib.dm_calc_cnt` counter.


.. literalinclude:: ../../tests/python/test_seq_dm_init.py
    :language: python


This script also expects existence of files with free-atomic density matrices in the ``ASI_FREE_ATOM_DMS``. Therefore 
before launching this script, we need to run the ``test_save_dm.py`` script for each atom that constitutes the 
computed molecule. Here we use acetaldehyde (CH3CHO) for example.

.. code-block:: bash

   # make files with free-atomic DMs in  /tmp:
   ASI_FREE_ATOM_DMS=/tmp python3 test_save_dm.py C
   ASI_FREE_ATOM_DMS=/tmp python3 test_save_dm.py O
   ASI_FREE_ATOM_DMS=/tmp python3 test_save_dm.py H

   
   # Finally compute the CH3CHO molecule:
   python3 test_seq_dm_init.py CH3CHO

The output should be the following:

.. code-block:: console

         Step     Time          Energy         fmax
  *Force-consistent energies used in optimization.
  LBFGS:    0 15:08:48    -4186.387365*       0.5310
  E = -4186.387365 eV. Number of SCF steps: 17
  LBFGS:    1 15:08:55    -4186.389899*       0.5155
  E = -4186.389899 eV. Number of SCF steps: 12
  LBFGS:    2 15:09:02    -4186.391687*       0.1266
  E = -4186.391687 eV. Number of SCF steps: 10
  LBFGS:    3 15:09:08    -4186.392083*       0.0863
  E = -4186.392083 eV. Number of SCF steps: 9
  LBFGS:    4 15:09:14    -4186.392245*       0.0505
  E = -4186.392245 eV. Number of SCF steps: 9
  LBFGS:    5 15:09:19    -4186.392336*       0.0404
  E = -4186.392336 eV. Number of SCF steps: 7
  LBFGS:    6 15:09:24    -4186.392567*       0.0409
  E = -4186.392567 eV. Number of SCF steps: 8
  LBFGS:    7 15:09:29    -4186.392741*       0.0408
  E = -4186.392741 eV. Number of SCF steps: 7
  LBFGS:    8 15:09:33    -4186.392911*       0.0347
  E = -4186.392911 eV. Number of SCF steps: 4
  LBFGS:    9 15:09:36    -4186.393041*       0.0309
  E = -4186.393041 eV. Number of SCF steps: 4
  LBFGS:   10 15:09:39    -4186.393118*       0.0252
  E = -4186.393118 eV. Number of SCF steps: 4
  LBFGS:   11 15:09:43    -4186.393115*       0.0109
  E = -4186.393115 eV. Number of SCF steps: 4
  LBFGS:   12 15:09:46    -4186.393098*       0.0080
  E = -4186.393098 eV. Number of SCF steps: 4

Here we see that number of SCF steps decays from 17 at the first geometry step, 
to 4 SCF steps after the 8-th geometry step. Total number of SCF steps is 99 here.
If you remove (or comment-out) three lines starting from ``dm_predictor.`` then you can meassure number
of SCF steps for convergence with default initial guess. Without the DM predictor the output
should be the following:

.. code-block:: console

           Step     Time          Energy         fmax
    *Force-consistent energies used in optimization.
    LBFGS:    0 15:16:03    -4186.387365*       0.5310
    E = -4186.387365 eV. Number of SCF steps: 16
    LBFGS:    1 15:16:10    -4186.389899*       0.5155
    E = -4186.389899 eV. Number of SCF steps: 11
    LBFGS:    2 15:16:16    -4186.391688*       0.1266
    E = -4186.391688 eV. Number of SCF steps: 10
    LBFGS:    3 15:16:22    -4186.392083*       0.0863
    E = -4186.392083 eV. Number of SCF steps: 9
    LBFGS:    4 15:16:28    -4186.392245*       0.0505
    E = -4186.392245 eV. Number of SCF steps: 9
    LBFGS:    5 15:16:33    -4186.392336*       0.0404
    E = -4186.392336 eV. Number of SCF steps: 8
    LBFGS:    6 15:16:39    -4186.392567*       0.0408
    E = -4186.392567 eV. Number of SCF steps: 8
    LBFGS:    7 15:16:45    -4186.392741*       0.0408
    E = -4186.392741 eV. Number of SCF steps: 9
    LBFGS:    8 15:16:51    -4186.392911*       0.0347
    E = -4186.392911 eV. Number of SCF steps: 9
    LBFGS:    9 15:16:56    -4186.393041*       0.0308
    E = -4186.393041 eV. Number of SCF steps: 8
    LBFGS:   10 15:17:02    -4186.393118*       0.0253
    E = -4186.393118 eV. Number of SCF steps: 8
    LBFGS:   11 15:17:07    -4186.393115*       0.0109
    E = -4186.393115 eV. Number of SCF steps: 8
    LBFGS:   12 15:17:13    -4186.393098*       0.0080
    E = -4186.393098 eV. Number of SCF steps: 8

Here we also see decrease in the number of SCF steps, because by default the DM of the previous
geometry step i used to initialize SCF loop for the next geomtery. But this approach is unable
to reduce number of step below 8 in this case. Here the total number of SCF steps is 121, that is 
about 20% more than with the :class:`asi4py.dm.PredictSeqDM` predictor.


`asi4py.dm` module reference
---------------------------------------------------------------------------------------

.. automodule:: asi4py.dm
   :members:
   :special-members: __init__,

