=====================================
Atomic Similation Interface (ASI) API
=====================================

Atomic Simulation Interface (ASI) is a native C-style API that includes functions for export and import of data structures that are used in electronic structure calculations and for classical molecular dynamics simulations. ASI aims to be a uniform, generic and efficient interface for connecting various computational chemistry and materials science codes in multiscale simulation workflows, such as QM/MM, QM/ML, QM/QM. ASI specifies functions, data types and calling conventions for export and import of density matrices, overlap and Hamiltonian matrices, electrostatic potential, atomic coordinates, charges, total energy and forces. 


`Full ASI API documentation <https://pvst.gitlab.io/asi>`__.

ASI API specification
---------------------

ASI API is specified as a C header file `asi.h`_. Codes implementing ASI API must provide linkable library with definitions of functions from `asi.h`_. Depending on particular usage of the implementaions, some functions can be ommited or implemented as stubs, if they are not going to used. To use Python ASI wrapper it is necessary to have all functions from `asi.h` defined, but of course stub definitions can be used.

.. _`asi.h`: https://pvst.gitlab.io/asi/asi_8h.html

`Doxygen-generated ASI API
specification <https://pvst.gitlab.io/asi/asi_8h.html>`__.

Implementations
----------------

-  `DFTB+ <https://dftbplus.org/>`__: `in separate
   branch <https://github.com/PavelStishenko/dftbplus/tree/ASI_v1.3>`__.
   ASI API is provided by a separate library that should be linked with DFTB+ library.
-  `FHI-aims <https://fhi-aims.org/>`__: in the main branch.

Supported platforms
-------------------

FHI-aims
~~~~~~~~

ASI API implementation in FHI-aims is routinely tested on Linux machines
with the following compilers and libraries:

-  Linux, GNU 7.5.0, Intel MKL 2021.4, and Intel MPICH 3.2.1.
-  Linux, Intel compilers, Intel MKL 2021.4, and Intel MPICH 3.2.1.
-  Debian Stable, GNU 9.3, OpenBLAS 0.3, and OpenMPI 4.1

DFTB+
~~~~~

Although the DFTB+ works on both Linux and MacOS machines, the ASI API
implementation is not tested for MacOS and build scripts are Linux-only.

The well-tested confuigurations are:

-  Debian Stable with GCC 9.3 and OpenMPI 4.1
-  Fedora 34 with GCC 11.3 and OpenMPI 4.1
-  Debian Testing with GCC 12.2 and OpenMPI 4.1

Python wrapper
--------------

ASI-enabled codes can be used in Python scripts via the ``asi4py``
wrapper of the ASI API.

To install asi4py use ``pip``

::

   pip install asi4py

:doc:`Read full asi4py reference <reference>`

Usage examples
---------------

See `tests/src` for examples of usage in a native code.

See `tests/python` for examples of usage in Python scripts.

:doc:`See tests descriptions for examples of a specific functionality <testing>`


A simple asi4py example
~~~~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../tests/python/example.py
    :code: python


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   ASI API reference <https://pvst.gitlab.io/asi/asi_8h.html>
   building
   testing
   reference
   ml_dm
   pubs_and_examples
   ackn
   licenses




Indices and tables
--------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
