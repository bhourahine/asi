===================================
Publications & examples
===================================


-----------------------------------
Publications
-----------------------------------

1. Pavel V. Stishenko, Thomas W. Keal, Scott M. Woodley, Volker Blum, Benjamin Hourahine, Reinhard J. Maurer, and Andrew J.Logsdail. `Atomic Simulation Interface (ASI): application programming interface for electronic structure codes <JOSS2023_>`_. *Journal of Open Source Software*, 8(85), 5186, (2023).

2. Pavel Stishenko, Adam McSloy, Berk Onat, Ben Hourahine, Reinhard J. Maurer, James R. Kermode, Andrew Logsdail. `Integrated workflows and interfaces for data-driven semi-empirical electronic structure calculations <JCP2024_>`_. *J. Chem. Phys.*, 7 July 2024; 161 (1): 012502. [`arXiv:2403.15625v2 <https://arxiv.org/abs/2403.15625v2>`_] [`Full text <https://pure.strath.ac.uk/ws/portalfiles/portal/222323677/Stishenko-etal-JCP-2024-Integrated-workflows-and-interfaces-for-data-driven-semi-empirical-electronic-structure.pdf>`_]

.. _JOSS2023: https://doi.org/10.21105/joss.05186
.. _JCP2024: https://doi.org/10.1063/5.0209742


-----------------------------------
Examples
-----------------------------------

.. contents:: Table of Contents
    :depth: 2
    :local:

Plotting band structure with DFTB+ 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../publication_examples/DFTBP_integr/simple_bands.py
    :code: python
    
    
Saving Hamiltonian and overlap matrices along a K-space path
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../publication_examples/DFTBP_integr/aims_bands.py
    :code: python
  

Plotting band structure with DFTB+ using Hamiltonians from FHI-aims
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../publication_examples/DFTBP_integr/imported_bands.py
    :code: python



