#ifndef ASI_H
#define ASI_H

/**
 * @file asi.h
 * @brief Atomic Simulation Interface (ASI) API
 
 This file contains declarations of ASI API functions and types and is 
 the primary reference of the ASI API.
 
 Changelog:
 
 1.0.0 Initial version
 
 1.1.0 Added ASI_version() function. Version bump for the JOSS paper.
 
 1.2.0 Added ASI_basis_atoms()
 
 1.3.0 Compatibility breaking release! Added special callback for 
       import: ASI_set_dmhs_callback_t returning if the matrix was changed in
       the callback. Added ASI_matrix_descr_t argument to describe matrix types.
       Added ASI_get_basis_func_descr() function replacing ASI_basis_atoms().
 */

#define ASI_VERSION_MAJOR 1
#define ASI_VERSION_MINOR 3
#define ASI_VERSION_PATCH 0

/*! Constant returned by \ref ASI_flavour() for FHI-aims implementation */
#define ASI_FHI_AIMS	1
/*! Constant returned by \ref ASI_flavour() for DFTB+ implementation */
#define ASI_DFTBP 	2

/*!
  Matrix descriptor for import/export callbacks.
  ASI implementations must document the set of supported types for each callback.
*/
typedef struct
{
  int matrix_type;   //!< Type of a matrix. See ASI_MATRIX_TYPE_* constants. For example \ref ASI_MATRIX_TYPE_HERMSYM
  int storage_type;  //!< Type of a matrix storage. See ASI_STORAGE_TYPE_* constants. For example \ref ASI_STORAGE_TYPE_TRIL
} ASI_matrix_descr_t;

/*! Generic matrix type for \ref ASI_matrix_descr_t::matrix_type  */
#define ASI_MATRIX_TYPE_GENERIC 0
/*! Hermitian or symmetric matrix type for \ref ASI_matrix_descr_t::matrix_type  */
#define ASI_MATRIX_TYPE_HERMSYM 1

/*! Dense matrix storage for \ref ASI_matrix_descr_t::storage_type  */
#define ASI_STORAGE_TYPE_DENSE_FULL 0
/*! Only lower triangle stored in column-major order (upper for row-major). For \ref ASI_matrix_descr_t::storage_type  */
#define ASI_STORAGE_TYPE_TRIL  1
/*! Only upper triangle stored in column-major order (lower for row-major). For \ref ASI_matrix_descr_t::storage_type  */
#define ASI_STORAGE_TYPE_TRIU  2



/*!
  Signature of callback functions for electrostatic potential (ESP) import.
  
  Signature of callback functions for electrostatic potential (ESP) import.
  Meant to be used with ASI_set_external_potential() or ASI_register_external_potential() functions.
  ESP sign is supposed to be as filled by positive charge.
  
  \param aux_ptr pointer to auxiliary object specified on callback registration.
  \param n number of points where the ESP is requested.
  \param coords pointer to array in row major format with shape `[n, 3]`. Contains coordinates of points where ESP is requested.
  \param potential pointer to array of size `n` or `NULL`. Points to output array, where ESP potential values should be placed.
                    If NULL, then ESP is not requested, that may be if ESP gradient is requested.
  \param potential_grad pointer to array in row major format with shape `[n, 3]` or `NULL`. 
                    Points to output array, where ESP potential gradient values should be placed. 
                    If NULL, then ESP gradient is not requested, that may be if ESP only is requested.
*/
typedef void (*ASI_ext_pot_func_t)(void *aux_ptr, int n, const double *coords, double *potential, double *potential_grad);

/*!
  Signature of callback functions for export of density, Hamiltonian or overlap matrices.
  
  Signature of callback functions for export of density (DM), Hamiltonian (H), or overlap (S) matrices.
  
  \param aux_ptr pointer to auxiliary object specified on callback registration.
  \param iK k-point index, 1-based.
  \param iS spin channel index, 1-based.
  \param blacs_descr pointer to BLACS array descriptor or `NULL`.
  \param blacs_data pointer to the exported matrix. Data type of the matrix depends on ASI_is_hamiltonian_real() value.
               Size of the matrix  depends on `blacs_descr` content or ASI_get_basis_size() value.
  \param matrix_descr pointer to descriptor of matrix structure and storage type \ref ASI_matrix_descr_t.
*/
typedef void (*ASI_dmhs_callback_t)(void *aux_ptr, int iK, int iS, int *blacs_descr, const void *blacs_data, ASI_matrix_descr_t *matrix_descr); // iK and iS are 1-based indices

/*!
  Signature of callback functions for import of density, Hamiltonian or overlap matrices.
  
  Signature of callback functions for import of density (DM), Hamiltonian (H), or overlap (S) matrices.
  
  \param aux_ptr pointer to auxiliary object specified on callback registration.
  \param iK k-point index, 1-based.
  \param iS spin channel index, 1-based.
  \param blacs_descr pointer to BLACS array descriptor or `NULL`.
  \param blacs_data pointer to the imported matrix. Data type of the matrix depends on ASI_is_hamiltonian_real() value.
               Size of the matrix  depends on `blacs_descr` content or ASI_get_basis_size() value.
  \param matrix_descr pointer to descriptor of matrix structure and storage type \ref ASI_matrix_descr_t.
  \return zero if matrix was not changes in the callback, non-zero otherwise.
*/
typedef int (*ASI_set_dmhs_callback_t)(void *aux_ptr, int iK, int iS, int *blacs_descr, void *blacs_data, ASI_matrix_descr_t *matrix_descr); // iK and iS are 1-based indices

/*!
  ASI version structure. Returned by the ASI_version() function
  major 
*/
typedef struct 
{
  int major;      //!< Major API changes. May break stable features
  int minor;      //!< Minor API changes. New unstable features are introduced or may be broken.
  int patch;      //!< Patches. Fixes of unstable features.
} ASI_version_t;


/*!
  Strucutre for description of basis function parameters. Returned by ASI_get_basis_func_descr()
  That structure is extendable and new types of descriptors may be implemented by various codes
  
  ** IMPORTANT **
  If you are going to add a new descriptor, please keep the fields `descr_size` and 
  `basis_func_descr_type` in the same places as here. 
  To avoid clashes of `basis_func_descr_type` values, please add new descriptor types in that 
  file via merge request at the repository here https://gitlab.com/pvst/asi/
*/
typedef struct
{
  int descr_size;              //!< Size of the descriptor in bytes
  int basis_func_descr_type;   //!< Type of the descriptor; 101 for ASI_basis_func_descr_generic_local_t.
} ASI_basis_func_descr_generic_t;


/*!
  Simple descriptor for atom-localised basis functions
*/
typedef struct
{
  int descr_size;              //!< Size of the descriptor in bytes
  int basis_func_descr_type;   //!< Type of the descriptor; 101 for that type.
  int atom_index;              //!< 0-based index of atom where the basis function is localised
} ASI_basis_func_descr_generic_local_t;


/*!
  Returns the supported version of the ASI API in runtime. 
  This function is defined in the `asi.h` header 
  and should NOT be implemented by ASI API implementations.
*/
extern inline ASI_version_t ASI_version()
{
  return (ASI_version_t){ASI_VERSION_MAJOR,ASI_VERSION_MINOR,ASI_VERSION_PATCH};
}

/*!
  Returns integer constant to unique for each API implementation
  \return Unique implementation identifier
*/
extern "C" int ASI_flavour();

/*!
  Initialize calculations
  
  User must generate all input files before ASI_init().
  Implementations must read input files here.
  No ASI API calls permitted before this function.
  
  \param inputpath path to file or directory with input files (configs, initial geometry, etc)
  \param outputfilename path to output file
  \param mpiComm MPI communicator in Fortran format. Can be obtained from MPI_Comm_c2f().
*/
extern "C" void ASI_init(const char *inputpath, const char *outputfilename, int mpiComm);

/*!
  Set coordinates of atoms
  
  Set coordinates of atoms. Coordinates are copied into internal storage, so the pointer do not have to be valid after return.
  \param coords pointer to array in row major format with shape [n_atoms, 3]. Contains new atomic coordinates.
  \param n_atoms number of atoms. Implementations may ignore this parameter if number if atoms is known at invocation moment.
*/
extern "C" void ASI_set_atom_coords(const double *coords, int n_atoms = -1);

/*!
  Set coordinates of atoms and lattice vectors
  
  Set coordinates of atoms and lattice vectors. Same as ASI_set_atom_coords() but with additional
  parameter for lattice vectors. Coordinates and lattice vectors are copied into internal storage, so the pointer do not have to be valid after return.
  
  \param coords pointer to array in row major format with shape [n_atoms, 3]. Contains new atomic coordinates.
  \param n_atoms number of atoms. Implementations may ignore this parameter if number if atoms is known at invocation moment.
  \param lattice pointer to array in row major format with shape [3, 3]. Contains new lattice vectors with vectors as rows.
*/
extern "C" void ASI_set_geometry(const double *coords, int n_atoms = -1, const double *lattice=0);

/*!
  Set external electrostatic potential (ESP).
  
  Set external electrostatic potential (ESP). The `callback` is not stored and will be invoked only before the function returns.
  
  \param callback callback function of \ref ASI_ext_pot_func_t type.
  \param aux_ptr auxiliary pointer that will be passed in every `callback` call.
*/
extern "C" void ASI_set_external_potential(ASI_ext_pot_func_t callback, void *aux_ptr);

/*!
  Register callback for external electrostatic potential (ESP) calculation.
  
  Register callback for external electrostatic potential (ESP) calculation. The `callback` will be stored and will be invoked during ASI_run() call.
  
  \param callback callback function of \ref ASI_ext_pot_func_t type.
  \param aux_ptr auxiliary pointer that will be passed in every `callback` call.
*/
extern "C" void ASI_register_external_potential(ASI_ext_pot_func_t callback, void *aux_ptr);

/*!
  Do actual calculations.
  
  Do actual calculations. Long-time calculations supposed to be done here. 
  Functions such as ASI_energy(), ASI_forces(), ASI_atomic_charges() must be invoked only after ASI_run()
*/
extern "C" void ASI_run();

/*!test_mpi
  Returns number of atoms
  
  Returns number of atoms
  \return number of atoms
*/
extern "C" int ASI_n_atoms();

/*!
  Returns total energy
  
  Returns total energy
  \return total energy
*/
extern "C" double ASI_energy();

/*!
  Returns forces acting on each atom
  
  Returns forces acting on each atom. May return `NULL` if forces calculation was not configured on ASI_init() call.
  For FHI-aims use `compute_forces .true.`. For DFTB+ use `Analysis { CalculateForces = Yes }`.
  
  \return pointer to array in row major format of shape `[ASI_n_atoms(), 3]` or `NULL`.
*/
extern "C" const double* ASI_forces();

/*!
  Returns stress tensor
  
  Returns stress tensor. May return `NULL` for non-periodic system or if stress calculation was not 
  configured on ASI_init() call.
  
  \return pointer to array in row major format of shape `[3, 3]` or `NULL`.
*/
extern "C" const double* ASI_stress();

/*!
  Return atomic charges
  
  Return atomic charges calculated by requested algorithm (charge partitioning scheme). 
  May return `NULL` if charge.partitioning was not configured on ASI_init() call.
  
  \return pointer to array of size `ASI_n_atoms()` or `NULL`.
*/
extern "C" const double* ASI_atomic_charges(int scheme=-1);

/*!
  Calculate electrostatic potential (ESP) for positive charges in specified coordinates.
  
  Calculate electrostatic potential (ESP) and/or ESP gradient for positive charges in specified coordinates.
  Parameters are similar to \ref ASI_ext_pot_func_t callback type.
  
  \param n number of points for which ESP calculation is requested
  \param coords pointer to array in row major format with shape `[n, 3]`. Contains coordinates of points where ESP is requested.
  \param potential pointer to array of size `n` or `NULL`. Points to output array, where ESP potential values should be placed.
                    If NULL, then ESP is not requested, that may be if ESP gradient is requested.
  \param potential_grad pointer to array in row major format with shape `[n, 3]` or `NULL`. 
                    Points to output array, where ESP potential gradient values should be placed. 
                    If NULL, then ESP gradient is not requested, that may be if ESP only is requested.
*/
extern "C" void ASI_calc_esp(int n, const double *coords, double *potential, double *potential_grad);

/*!
  Returns number of spin channels.
  
  Returns number of spin channels. Returns 1 for spin-paired calculations.
  
  \return number of spin channels. Returns 1 for spin-paired calculations.
*/
extern "C" int ASI_get_nspin();

/*!
  Returns number of k-points.
  
  Returns number of k-points. Returns 1 for non-periodic or Gamma-point calculations.
  
  \return number of k-points. Returns 1 for non-periodic or Gamma-point calculations.
*/
extern "C" int ASI_get_nkpts();

/*!
  Returns number of (k-point, spin channel) pairs processed by current process.
  
  Meant to be used in case of distributed (MPI) calculation, when parallelization is performed along k-points 
  and spin channels. Returns number of (k-point, spin channel) pairs processed by current process.
  
  \return number of (k-point, spin channel) pairs processed by current process.
*/
extern "C" int ASI_get_n_local_ks();

/*!
  Returns list of (k-point, spin channel) pairs processed by current process.
  
  Meant to be used in case of distributed (MPI) calculation, when parallelization is performed along k-points 
  and spin channels. Returns list and number of (k-point, spin channel) pairs processed by current process.
  
  \param local_ks pointer to output array. 
                  `local_ks[0, i] == i_kpnt`, 
                  
                  `local_ks[1, i] == i_spin`, 
                  
                  `i < ASI_get_n_local_ks()`
                  
                  `i_kpnt` and `i_kpnt` are 1-based indices
  \return number of (k-point, spin channel) pairs processed by current process.
*/
extern "C" int ASI_get_local_ks(int *local_ks);

/*!
  Returns basis set size
  
  Returns basis set size. Defines total size of DM, H, and S matrices passed into callbacks of \ref ASI_dmhs_callback_t type.
  Note, that in case of distributed calculation local matrix size is defined by BLACS descriptor.
  \return basis set size. 
*/
extern "C" int ASI_get_basis_size();

/*!
  Returns a pointer to a basis function descriptior.
  
  \param basis_func_index zero-based index of a basis function
  \return a pointer to a `ASI_basis_func_descr_generic_t` structure or any derived type,
          such as `ASI_basis_func_descr_generic_local_t`. The pointer is valid until 
          next ASI call. Pointer ownership is not transferred by this call.
*/
extern "C" ASI_basis_func_descr_generic_t* ASI_get_basis_func_descr(int basis_func_index);

/*!
  If hamiltonians, overlap, and density matrices are real.
  
  Returns True if hamiltonians, overlap, and density matrices are real. Returns False otherwise.
  It defines type of matrices passed into callbacks of \ref ASI_dmhs_callback_t type.

  \return if hamiltonians, overlap, and density matrices are real.
*/
extern "C" bool ASI_is_hamiltonian_real();

/*!
  Register callback for density matrix evaluation.
  
  Register callback for density matrix evaluation. 
  Callback will be invoked on each density matrix evaluation, including evaluations within SCF loop.
  Note, that FHI-aims evaluates density matrix in SCF loop only if option `density_update_method density_matrix` is in config file.
  Note, that if FHI-aims evaluates forces then the last evaluations of the density matrix do not correspond to the calculated geometry.
  
  \param callback callback of type \ref ASI_dmhs_callback_t
  \param aux_ptr auxiliary pointer that will be passed in every `callback` call.
*/
extern "C" void ASI_register_dm_callback(ASI_dmhs_callback_t callback, void *aux_ptr);

/*!
  Register callback for overlap matrix evaluation.
  
  Register callback for overlap matrix evaluation. 
  Callback will be invoked on each overlap matrix evaluation. Even if overlap matrix is the same for all k-point and spin channels, 
  the callback will be invoked for each (k-point, spin channel) pair. It is up to client to decide when is the best time to work with the matrix. 
  Computational cost of callback invocation is negligible if no work is done in callback itself.
  
  \param callback callback of type \ref ASI_dmhs_callback_t
  \param aux_ptr auxiliary pointer that will be passed in every `callback` call.
*/
extern "C" void ASI_register_overlap_callback(ASI_dmhs_callback_t callback, void *aux_ptr);

/*!
  Register callback for overlap matrix input.
  
  Register callback for overlap matrix input.
  Callback is expected to provide overlap matrix.
  
  \param callback callback of type \ref ASI_set_dmhs_callback_t
  \param aux_ptr auxiliary pointer that will be passed in every `callback` call.
*/
extern "C" void ASI_register_set_overlap_callback(ASI_set_dmhs_callback_t callback, void *aux_ptr);

/*!
  Register callback for hamiltonian matrix evaluation.
  
  Register callback for hamiltonian matrix evaluation. 
  Callback will be invoked on each hamiltonian matrix evaluation.
  
  \param callback callback of type \ref ASI_dmhs_callback_t
  \param aux_ptr auxiliary pointer that will be passed in every `callback` call.
*/
extern "C" void ASI_register_hamiltonian_callback(ASI_dmhs_callback_t callback, void *aux_ptr);

/*!
  Register callback for hamiltonian matrix input.
  
  Register callback for hamiltonian matrix input.
  Callback is expected to provide hamiltonian matrix.
  
  \param callback callback of type \ref ASI_set_dmhs_callback_t
  \param aux_ptr auxiliary pointer that will be passed in every `callback` call.
*/
extern "C" void ASI_register_set_hamiltonian_callback(ASI_set_dmhs_callback_t callback, void *aux_ptr);

/*!
  Register callback for density matrix initial guess.
  
  Register callback for density matrix initial guess. Callback is expected to fill the matrix argument with initial guess of the density matrix.
  It is a good place for calculation restart or density matrix prediction.
  
  \param callback callback of type \ref ASI_dmhs_callback_t
  \param aux_ptr auxiliary pointer that will be passed in every `callback` call.
*/
extern "C" void ASI_register_dm_init_callback(ASI_set_dmhs_callback_t callback, void *aux_ptr);

/*!
  Finalize calculation.
  
  Free memory, close opened files. Library may be unloaded after that. No API calls are permitted after that call.
*/
extern "C" void ASI_finalize();
// no calls after ASI_finalize

#endif // ASI_H
